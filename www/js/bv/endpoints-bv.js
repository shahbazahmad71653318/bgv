
// POST Request for Specification Amend
function postSpecificationAmendRequest(payload, callback) {
  hf.postData(true, "CertificateofSpecificationAmendAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Request for Ship Deletion Certificate
function postShipDeletionRequest(payload, callback) {
  hf.postData(true, "ShipDeletionAddItem/", payload, function (data) {
    callback(data);
  });
}

// GET Vessel Details
function getVesselDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "VesselGetItemByID/", params, function (data) {

    callback(data);
  });
}

// POST Request for To Whom Concern
function postToWhomConcernRequest(payload, callback) {
  hf.postData(true, "ToWhomConcernAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Request for To Whom Concern
function updateToWhomConcernRequest(payload, callback) {
  hf.postData(true, "ToWhomConcernUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// POST Request for Safety Insurance
function postSafetyInsuranceRequest(payload, callback) {
  hf.postData(true, "SafetyInsuranceAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Certificate Extension
function postCertificateExtensionRequest(payload, callback) {
  hf.postData(true, "ExtensionCertificateAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST NOL Certificate
function postNOLCertificateRequest(payload, callback) {
  hf.postData(true, "NOLRequestAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST NOL Certificate
function postLostReplacementRequest(payload, callback) {
  hf.postData(true, "LostReplacementAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Exemption Certificate
function postExemptionRequest(payload, callback) {
  hf.postData(true, "ExemptionCertificateAddItem/", payload, function (data) {
    callback(data);
  });
}

// GET All Vessels
function getAllVessels(payload, callback) {
  hf.postData(true, "VesselGetAllItems/", payload, function (data) {
    callback(data);
  });
}

// POST Derating Certificate
function postDeratingCertificateRequest(payload, callback) {
  hf.postData(true, "DeratingCertificateAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Marine Incident Request
function postMarineIncidentRequest(payload, callback) {
  hf.postData(true, "MarineIncidentAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Mortrage Request
function postMortrageRequest(payload, callback) {
  hf.postData(true, "MortgageRegistrationAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST Safe Manning Request
function postSafeManningRequest(payload, callback) {
  hf.postData(true, "SafeManningAddItem/", payload, function (data) {
    callback(data);
  });
}

// POST CSR Request
function postCSRRequest(payload, callback) {
  hf.postData(true, "ContinuousSynopsisRecordAddItem/", payload, function (data) {
    callback(data);
  });
}

// My Vessels
function getMyVessels(VesselID, callback) {
  console.log(VesselID);
  //params.itemID = VesselID;
  hf.postData(true, "OwnerVessels/", {}, function (data) {

    callback(data);
  });
}

// GET Exemption Details
function getExemptionDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "ExemptionCertificateGetItemByID/", params, function (data) {

    callback(data);
  });
}

// UPDATE Exemption Details
function updateExemptionDetails(payload, callback) {
  hf.postData(true, "ExemptionCertificateUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Extension Details
function getExtensionDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "ExtensionCertificateGetItemByID/", params, function (data) {

    callback(data);
  });
}

// UPDATE Extension Details
function updateExtensionDetails(payload, callback) {
  hf.postData(true, "ExtensionCertificateUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Safety Insurance Details
function getSafetyInsuranceDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "SafetyInsuranceGetItemByID/", params, function (data) {

    callback(data);
  });
}

// UPDATE Safety Insurance Details
function updateSafetyInsuranceDetails(payload, callback) {
  hf.postData(true, "SafetyInsuranceUpdateItem/", payload, function (data) {
    callback(data);
  });
}


// GET CSR Details
function getCSRDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "ContinuousSynopsisRecordGetItemByID/", params, function (data) {

    callback(data);
  });
}

// UPDATE CSR Details
function updateCSRDetails(payload, callback) {
  hf.postData(true, "ContinuousSynopsisRecordUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Derrating Details
function getDerratingDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "DeratingCertificateGetItemByID/", params, function (data) {

    callback(data);
  });
}

// UPDATE Derrating Details
function updateDerratingDetails(payload, callback) {
  hf.postData(true, "DeratingCertificateUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Marine Incident Details
function getMarineIncidentDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "MarineIncidentGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Marine Incident Details
function updateMarineIncidentDetails(payload, callback) {
  hf.postData(true, "MarineIncidentUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET NOL Details
function getNOLDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "NOLRequestGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE NOL Details
function updateNOLDetails(payload, callback) {
  hf.postData(true, "NOLRequestUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET To Whom Details
function getToWhomDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "ToWhomConcernGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE To Whom Details
function updateToWhomDetails(payload, callback) {
  hf.postData(true, "ToWhomConcernUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// UPLOAD FIle
function uploadFile(payload, callback) {
  $.ajax({
    type: "POST",
    url: SERVER_URL + 'UploadFiles',
    processData: false,
    contentType: false,
    async: false,
    header: { 'Authorization': 'Bearer ' + token },
    data: payload,
    success: function (result) {
      callback(result);
    },
    error: function (err) {

      console.log("err: " + err);
    }
  });
}

// UPLOAD FIle Add Document
function uploadDocumentAddFile(payload, callback) {
  hf.postData(true, "UploadedDocumentsAddFile/", payload, function (data) {
    callback(data);
  });
}

//DELETE File
function deleteFile(payload, callback) {
  hf.postData(true, "DeleteFile/", payload, function (data) {
    callback(data);
  });
}


// GET Lost & Replacement Details
function getLostAndReplacementDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "LostReplacementGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Lost & Replacement Details
function updateLostAndReplacementDetails(payload, callback) {
  hf.postData(true, "LostReplacementUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Ship Deletion Details
function getShipDeletionDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "ShipDeletionGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Ship Deletion Details
function updateShipDeletionDetails(payload, callback) {
  hf.postData(true, "ShipdeletionUpdateitem/", payload, function (data) {
    callback(data);
  });
}

// POST Ownership Transfer Details
function postOwnershipTransferDetails(payload, callback) {
  hf.postData(true, "TransferOwnershipAddItem/", payload, function (data) {
    callback(data);
  });
}

// GET Ownership Transfer Details
function getOwnershipTransferDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "TransferOwnershipGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Ownership Transfer Details
function updateOwnershipTransferDetails(payload, callback) {
  hf.postData(true, "TransferOwnershipUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Specification Amend Details
function getSpecificationAmendDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "CertificateofSpecificationAmendGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Specification Amend Details
function updateSpecificationAmendDetails(payload, callback) {
  hf.postData(true, "CertificateofSpecificationAmendUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Mortgage Details
function getMortgageDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "MortgageRegistrationGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Mortgage Details
function updateMortgageDetails(payload, callback) {
  hf.postData(true, "MortgageRegistrationUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Mortgage Details
function getMortgageDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "MortgageRegistrationGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Mortgage Details
function updateMortgageDetails(payload, callback) {
  hf.postData(true, "MortgageRegistrationUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// GET Non Ecumbrance Details
function getNonEncumbranceDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "NonEncumbranceGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Non Ecumbrance Details
function updateNonEncumbranceDetails(payload, callback) {
  hf.postData(true, "NonEncumbranceUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// POST Non Ecumbrance Details
function postNonEncumbranceDetails(payload, callback) {
  hf.postData(true, "NonEncumbranceAddItem/", payload, function (data) {
    callback(data);
  });
}

// GET Non Ecumbrance Certificate Details
function getNonEncumbranceCertDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "CertificateofNonEncumbranceGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Non Ecumbrance Certificate Details
function updateNonEncumbranceCertDetails(payload, callback) {
  hf.postData(true, "CertificateofNonEncumbranceUpdateItem/", payload, function (data) {
    callback(data);
  });
}

// POST Non Ecumbrance Certificate Details
function postNonEncumbranceCertDetails(payload, callback) {
  hf.postData(true, "CertificateofNonEncumbranceAddItem/", payload, function (data) {
    callback(data);
  });
}

// GET Safe Manning Details
function getSafeManningDetails(VesselID, callback) {
  var params = {};
  params.itemID = VesselID;
  hf.postData(true, "SafeManningGetItemByID/", params, function (data) {
    callback(data);
  });
}

// UPDATE Safe Manning Details
// function updateNonEncumbranceCertDetails(payload, callback) {
//   hf.postData("SafeManningUpdateItem/", payload, function (data) {
//     callback(data);
//   });
// }

// GET Owner Details
function getOwnerDetails(itemId, callback) {
  var params = {};
  params.itemID = itemId;
  hf.postData(true, "OwnerData/", {}, function (data) {
    callback(data);
  });
}

// GET Non Ecumbrance Certificate Details
function getAttachment(file, callback) {
  var params = {};
  params = file;
  hf.postData(true, "UploadedDocumentsGetItemByFileTypeAndGUID/", params, function (data) {
    callback(data);
  });
}

// GET PORTS
function getPorts(callback) {
  hf.postData(true, "PortGetAllItems/", function (data) {
    callback(data);
  });
}

// GET INSURERS
function getInsurers(callback) {
  hf.postData(true, "InsuranceGetAllItems/", function (data) {
    callback(data);
  });
}

// GET Certificates List
function getCertificateList(callback) {
  hf.postData(true, "VesselServicesGetAllItems/", function (data) {
    callback(data);
  });
}

// GET Certificates List
function getAllRequestsByOwnerID(body, callback) {
  var params = body;

  hf.postData(true, "GetAllRequestsByOwnerID/", params, function (data) {
    callback(data);
  });
}

// GET Certificates List
function getAllInsurers(body, callback) {
  var params = body;
  hf.postData(true, "InsuranceGetAllItems/", params, function (data) {
    callback(data);
  });
}

// GET Certificates List
function getAllInsurers(body, callback) {
  var params = body;
  hf.postData(true, "InsuranceGetAllItems/", params, function (data) {
    callback(data);
  });
}

// Add Owner
function addOwner(body, callback) {
  var params = body;
  hf.postData(true, "OwnerAddItem/", params, function (data) {
    callback(data);
  });
}

// Get Owner
function getOwner(body, callback) {
  var params = body;
  hf.postData(true, "OwnerData/", params, function (data) {
    callback(data);
  });
}

// Get Inspector Comments
function getInspectorComments(body, callback) {
  var params = body;
  hf.postData(true, "InspectorComments/", params, function (data) {
    callback(data);
  });
}

// Get Fess of Service
function getServiceFee(body, callback) {
  var params = body;
  hf.postData(true, "GetFeeByServiceID/", params, function (data) {
    callback(data);
  });
}

// Get Request Exist
function requestIsExist(body, callback) {
  var params = body;
  hf.postData(true, "RequestIsExist/", params, function (data) {
    callback(data);
  });
}

// Get Request Exist
function nolRequestAddNationality(body, callback) {
  var params = body;
  hf.postData(true, "NOLRequestAddNationality/", params, function (data) {
    callback(data);
  });
}

function nolRequestAddNatureOfOperation(body, callback) {
  var params = body;
  hf.postData(true, "NOLRequestAddNatureOfOperation/", params, function (data) {
    callback(data);
  });
}

function nolRequestGetNationality(body, callback) {
  var params = body;
  hf.postData(true, "NOLRequestGetNationality/", params, function (data) {
    callback(data);
  });
}

function natureOfOperationGetItemByID(body, callback) {
  var params = body;
  hf.postData(true, "NOLRequestGetNatureOfOperation/", params, function (data) {
    callback(data);
  });
}

function getOwnerByQid(body, callback) {
  var params = body;
  hf.postData(true, "GetOwnerIDByQID/", params, function (data) {
    callback(data);
  });
}

function payment(payload, callback) {
  //var params = body;
  hf.postData('payment', "SendPaymentAction", payload, function (data) {
    callback(data);
  });
}

function getPaymentData(payload, callback) {
  hf.postData(true, "GetPaymentData", payload, function (data) {
    callback(data);
  });
}

function getAllCountries(callback) {
  hf.postData(true, "GetAllCountries/", function (data) {
    callback(data);
  });
}

function getAllInsuranceType(callback) {
  hf.postData(true, "InsuranceTypeGetAllItems/", function (data) {
    callback(data);
  });
}

function getAllOwners(callback) {
  hf.postData(true, "GetAllOwners/", function (data) {
    callback(data);
  });
}

function insuranceGetItemByID(body, callback) {
  var params = body;
  hf.postData(true, "InsuranceGetItemByID/", params, function (data) {
    callback(data);
  });
}

function insuranceDetailsAddItem(body, callback) {
  var params = body;
  hf.postData(true, "InsuranceDetailsAddItem/", params, function (data) {
    callback(data);
  });
}

function insuranceUpdateAddress(body, callback) {
  var params = body;
  hf.postData(true, "InsuranceUpdateItem/", params, function (data) {
    callback(data);
  });
}


function insuranceDetailsGetItemById(body, callback) {
  var params = {};
  params.RequestID = body;
  hf.postData(true, "InsuranceDetailsGetItemById/", params, function (data) {
    callback(data);
  });
}

function getBadgesCountersByOwnerID(callback) {
  var params = {};
  hf.postData(true, "GetBadgesCountersByOwnerID/", params, function (data) {
    callback(data);
  });
}

function tasklistAddItem(body, callback) {
  var params = {};
  params.RequestID = body;
  hf.postData(true, "TaskListAddItem/", params, function (data) {
    callback(data);
  });
}

function tasklistUpdateItem(body, callback) {
  var params = {};
  params.RequestID = body;
  hf.postData(true, "TaskListUpdateItem/", params, function (data) {
    callback(data);
  });
}

function getAllClasses(callback) {
  hf.postData(true, "getallclasses/", function (data) {
    callback(data);
  });
}

function deleteNatureOfOperation(body, callback) {
  var params = {};
  params.itemID = body;
  hf.postData(true, "NOLRequestDeleteNatureOfOperation/", params, function (data) {
    callback(data);
  });
}
