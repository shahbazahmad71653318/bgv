function setExemptionAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#exemption-attachment-letter');

    file.FileType = 'Class Certificate';
    formatAttachment(file, '#exemption-attachment-class');

    file.FileType = 'Subject Certificate';
    formatAttachment(file, '#exemption-attachment-subject');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#exemption-attachment-payment');
}

function setExtensionAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#authorization-attachment-letter');

    file.FileType = 'Class Certificate';
    formatAttachment(file, '#authorization-attachment-class');

    file.FileType = 'Subject Certificate';
    formatAttachment(file, '#authorization-attachment-subject');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#authorization-attachment-payment');
}

function setSafetyInsuranceAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#insurance-attachment-letter');

    file.FileType = 'Policy of Insurance';
    formatAttachment(file, '#insurance-attachment-policy');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#insurance-attachment-payment');
}

function setNonEncumbranceCertAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#certNonEnc-attachment-letter');

    file.FileType = 'Original Certificate';
    formatAttachment(file, '#certNonEnc-attachment-original');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#certNonEnc-attachment-payment');
}

function setCSRAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#csr-attachment-letter');

    file.FileType = 'Original Certificate of Qatar Registry';
    formatAttachment(file, '#csr-attachment-registry');

    file.FileType = 'Document of Appliance';
    formatAttachment(file, '#csr-attachment-appliance');

    file.FileType = 'Safety Management Certificate';
    formatAttachment(file, '#csr-attachment-safety');

    file.FileType = 'International Ship Security Certificate';
    formatAttachment(file, '#csr-attachment-security');

    file.FileType = 'CSR from another flag';
    formatAttachment(file, '#csr-attachment-flag');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#csr-attachment-payment');
}

function derratingAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#derating-attachment-letter');

    file.FileType = 'Copy Of The Rodent Fee Test Report';
    formatAttachment(file, '#derating-attachment-rodent');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#derating-attachment-payment');
}

function marineAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Log Book"
    }
    formatAttachment(file, '#marine-attachment-log');

    file.FileType = 'Captain Report';
    formatAttachment(file, '#marine-attachment-report');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#marine-attachment-payment');
}

function nolAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Official From Owner"
    }
    formatAttachment(file, '#nol-attachment-letter');

    file.FileType = 'Copy of Registry Certificates for Flag Vessels involved';
    formatAttachment(file, '#nol-attachment-flag');

    file.FileType = 'Copy of Work Permit for Foreign Vessels involves';
    formatAttachment(file, '#nol-attachment-foreign');

    file.FileType = 'Copy of Navigational Chart for the Work Site';
    formatAttachment(file, '#nol-attachment-navigational');

    file.FileType = 'Proposal for the Announcement in Arabic';
    formatAttachment(file, '#nol-attachment-arabic');

    file.FileType = 'Proposal for the Announcement in English';
    formatAttachment(file, '#nol-attachment-english');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#nol-attachment-payment');
}

function toWhomAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#whom-attachment-letter');

    file.FileType = 'Copy of Certificate of Qatar Registry';
    formatAttachment(file, '#whom-attachment-certificate');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#whom-attachment-payment');
}

function lostAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#lost-attachment-letter');

    file.FileType = 'Copy of Certificate of Qatar Registry';
    formatAttachment(file, '#lost-attachment-certificate');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#lost-attachment-payment');
}

function transferAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#transfer-attachment-letter');

    file.FileType = 'Original Registry Certificate';
    formatAttachment(file, '#transfer-attachment-certificate');

    file.FileType = 'QID Copy (Seller)';
    formatAttachment(file, '#transfer-attachment-qidSeller');

    file.FileType = 'Commercial Registration (Seller)';
    formatAttachment(file, '#transfer-attachment-commercialSeller');

    file.FileType = 'QID Copy (Buyer)';
    formatAttachment(file, '#transfer-attachment-qidBuyer');

    file.FileType = 'Commercial Registration (Buyer)';
    formatAttachment(file, '#transfer-attachment-commercialBuyer');

    file.FileType = 'Bill of Sale';
    formatAttachment(file, '#transfer-attachment-bill');

    file.FileType = 'Ship building Certificate';
    formatAttachment(file, '#transfer-attachment-shipBuilding');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#transfer-attachment-payment');
}

function mortgageAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#mortgage-attachment-letter');

    file.FileType = 'Letter from Bank';
    formatAttachment(file, '#mortgage-attachment-letterBank');

    file.FileType = 'Mortgage Contract';
    formatAttachment(file, '#mortgage-attachment-contract');

    file.FileType = 'Original Certificate of Qatar Registry';
    formatAttachment(file, '#mortgage-attachment-original');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#mortgage-attachment-payment');
}

function specificationAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#specification-attachment-letter');

    file.FileType = 'Commercial Registration';
    formatAttachment(file, '#specification-attachment-commercial');

    file.FileType = 'Letter from the Bank';
    formatAttachment(file, '#specification-attachment-bank');

    file.FileType = 'Previous Flag registration Certificate';
    formatAttachment(file, '#specification-attachment-flag');

    file.FileType = 'Certificate of Classification';
    formatAttachment(file, '#specification-attachment-classification');

    file.FileType = 'International Tonnage certificate';
    formatAttachment(file, '#specification-attachment-tonnage');

    file.FileType = 'International Load Line Certificate';
    formatAttachment(file, '#specification-attachment-loadLine');

    file.FileType = 'Cargo ship Safety Radio Certificate';
    formatAttachment(file, '#specification-attachment-cargoRadio');

    file.FileType = 'Documents of Compliance';
    formatAttachment(file, '#specification-attachment-compliance');

    file.FileType = 'Safety Management Certificate';
    formatAttachment(file, '#specification-attachment-safety');

    file.FileType = 'Cargo Ship Safety Construction Certificate';
    formatAttachment(file, '#specification-attachment-cargoConstruction');

    file.FileType = 'Cargo Ship Safety Equipment Certificate';
    formatAttachment(file, '#specification-attachment-cargoSafety');

    file.FileType = 'International Oil Pollution Prevention Certificate';
    formatAttachment(file, '#specification-attachment-oilPollution');

    file.FileType = 'International Sewage Pollution Prevention Certificate';
    formatAttachment(file, '#specification-attachment-sewagePollution');

    file.FileType = 'International Air Pollution Prevention Certificate';
    formatAttachment(file, '#specification-attachment-airPollution');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#specification-attachment-payment');
}


function nonEncumbranceAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#encumbrance-attachment-letter');

    file.FileType = 'Letter from Bank';
    formatAttachment(file, '#encumbrance-attachment-letterBank');

    file.FileType = 'Original Certificate of Qatar Registry';
    formatAttachment(file, '#encumbrance-attachment-original');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#encumbrance-attachment-payment');
}

function deletionAttachments(id) {
    let file = {
        "FileGUID": id,
        "FileType": "Letter from Owner"
    }
    formatAttachment(file, '#deletion-attachment-letter');

    file.FileType = 'Original Certificate of Qatar Registry';
    formatAttachment(file, '#deletion-attachment-certificate');

    file.FileType = 'Letter from New flag Country';
    formatAttachment(file, '#deletion-attachment-flag');

    file.FileType = 'Bill of Sale';
    formatAttachment(file, '#deletion-attachment-bill');

    file.FileType = 'Evidence of ship Scrap or Sink';
    formatAttachment(file, '#deletion-attachment-evidence');

    file.FileType = 'Payment Receipt';
    formatAttachment(file, '#encumbrance-attachment-payment');
}

function formatAttachment(file, id) {
    getAttachment(file, function (fileName) {
        fileName = fileName.Result[0].FileURL;
        $$(id).val(fileName.split('/')[4]);
    })
}