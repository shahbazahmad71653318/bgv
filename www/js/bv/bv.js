//  Templates 

var vesselTPL_template = Template7.partials.vesselInfo.template;
var vesselTPL = Template7.compile(vesselTPL_template);

var vesselDIM_template = Template7.partials.vesselDimension.template;
var vesselDIM_TPL = Template7.compile(vesselDIM_template);

var engineSpecs_template = Template7.partials.engineSpecs.template;
var engineSpecs_TPL = Template7.compile(engineSpecs_template);

var engineSpecsTextbox_template = Template7.partials.engineSpecsTextbox.template;
var engineSpecsTextbox_TPL = Template7.compile(engineSpecsTextbox_template);


var vesselDIMTextbox_template = Template7.partials.vesselDIMTextbox.template;
var vesselDIMTextbox_TPL = Template7.compile(vesselDIMTextbox_template);

var myVessels_template = Template7.partials.myVessels.template;
var myVessels_TPL = Template7.compile(myVessels_template);


var myRequests_template = Template7.partials.myRequests.template;
var myRequests_TPL = Template7.compile(myRequests_template);

var ownerRequests_template = Template7.partials.ownerRequests.template;
var ownerRequests_TPL = Template7.compile(ownerRequests_template);

var vesselsCertificate_template = Template7.partials.vesselsCertificate.template;
var vesselsCertificate_TPL = Template7.compile(vesselsCertificate_template);

var vesselDetails_template = Template7.partials.vesselDetails.template;
var vesselDetails_TPL = Template7.compile(vesselDetails_template);

var tasksList_template = Template7.partials.tasksList.template;
var tasksList_TPL = Template7.compile(tasksList_template);

var buyers_template = Template7.partials.buyersDetail.template;
var buyers_TPL = Template7.compile(buyers_template);

var marine_template = Template7.partials.marinecraft.template;
var marine_TPL = Template7.compile(marine_template);

var insurer_template = Template7.partials.insurerDetails.template;
var insurer_TPL = Template7.compile(insurer_template);

var isFile = false;
var vesselServices = {};
var requestStatus = '';
var ownerDetails_TPL, myDetails_TPL;
var nolMarineEvent;
var insurerEvent;
var ownerNewOrExistingBuyers = 'New';
var nationalityArray = [];
var nationalityArrayUpdate = [];
var isQatariRegistration = '1';
var requestPageEvent = {};
var taskPageEvent = {};
var allRequests = [];
var tasksAll = [];
var natureOfOperationArr = [];
var attachmentsGuid = '';

// My Details
$$(document).on('page:init', '.page[data-name="my-details"]', function (e) {
    setLanguage();
    setOwnerDetails(e);
});
$$(document).on('page:init', '.page[data-name="home-bgv"]', function (e) {
    console.log("test");


});

// Vessel Details
$$(document).on('page:init', '.page[data-name="vessel-details"]', function (e) {

    setLanguage();

    vw = app.views.current;

    let vesselId = vw.router.currentRoute.params.vesselID;

    getVesselDetails(vesselId, function (data) {


        var vesselDetails = vesselDetails_TPL(data.Result[0]);

        var t = e.detail.$el[0];

        $$(t).find(".vessel_details").html(vesselDetails);

        if (data.Result[0].TypeofVessel === 'barge') {
            $$('#engine-particular-details').hide();
        }


    })

});

// Page Issue-non-encumbrance
$$(document).on('page:init', '.page[data-name="issue-non-encumbrance"]', function (e) {

    setLanguage();

    isFile = false;

    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);

        // if (JSON.parse(data).Result[0].TypeofVessel === 'barge') {
        if (data.Result[0].TypeofVessel === 'barge') {
            $$('#engineSpecsInner').hide();
            $$('#engineSpecsTextboxInner').hide();
        }
    })
    $$('#request-status-container').hide();
    $$('#comments-section').hide();
    $$('#cancel').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {

        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getNonEncumbranceCertDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                data = data.Result[0];
                localStorage.setItem('Request-status', data.RequestStatus);

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.CommentsToVesselOwner;

                }
                if (data.RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.FlagInspectorComments;
                }
                if (data.RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }


                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                // $$('#certificate-receiving-data').val('11/07/2018');
                $$('#certificate-receiving-data').val(moment(data.CertificateReceivingDate).format('YYYY-MM-DD'));
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.RequestStatus;
                attachmentsGuid = data.AttachmentID;
                setNonEncumbranceCertAttachments(data.AttachmentID);

                if (data.RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-certNonEnc').hide();
                    $$('#clear').hide();
                }
            })
        }

        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            $$('#submit-certNonEnc').hide();
            $$('#clear').hide();


        }
    }, 1000)

    $$(document).on('click', '#submit-certNonEnc', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = vesselId;
        payload.AttachmentId = attachmentsGuid;
        payload.CertificateReceivingDate = moment($$('#certificate-receiving-data').val()).format('MM/DD/YYYY')
        console.log(payload);

        console.log(moment($$('#certificate-receiving-data').val()).format('MM/DD/YYYY'));
        // moment($$('#certificate-receiving-data').val()).format('MM/DD/YYYY');

        attachments.letter = $$('#certNonEnc-attachment-letter').val();

        if (!attachments.letter) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (isEdit) {
            payload.itemID = localStorage.getItem('itemId');
            if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
            }
            if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
            }
            if (e.handled !== true) { // This will prevent event triggering more then once
                e.handled = true;
                updateNonEncumbranceCertDetails(payload, function (data) {
                    // var data = JSON.parse(data)

                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }

        } else {
            if (e.handled !== true) { // This will prevent event triggering more then once
                e.handled = true;
                postNonEncumbranceCertDetails(payload, function (data) {
                    // var data = JSON.parse(data)

                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }

        }
    })

    $$(document).on('click', '#cancel-nonEn', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateNonEncumbranceCertDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }


    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// Specification Amend Page
$$(document).on('page:init', '.page[data-name="sepecification-amend"]', function (e) {

    setLanguage();

    isFile = false;
    let typeOfVessel = '';
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);

        typeOfVessel = data.Result[0].TypeofVessel;
        setTimeout(function () {
            // $$('#ShipAgencyVesselNameTextbox').val(JSON.parse(data).Result[0].Name);
            $$('#ShipAgencyVesselNameTextbox').val(data.Result[0].Name);
            // $$('#engineMaker').val(JSON.parse(data).Result[0].EngineMaker);
            $$('#engineMaker').val(data.Result[0].EngineMaker);
        }, 2000);

        setTimeout(function () {
            if (data.Result[0].TypeofVessel === 'barge') {
                $$('#pollution').hide();
                $$('#sewage').hide();
                $$('#oil').hide();
                $$('#cargo-equip').hide();
                $$('#cargo-construction').hide();
                $$('#safety').hide();
                $$('#compliance').hide();
                $$('#cargo').hide();
            }
            // if (JSON.parse(data).Result[0].IsMortgaged === false) {
            if (data.Result[0].IsMortgaged === false) {
                $$('#bank-letter').hide();
            } else {
                $$('#bank-letter').show();
            }

        }, 1000)

        let file = {
            "FileGUID": data.Result[0].AttachmentID,
            "FileType": "QID Copy"
        }
        getAttachment(file, function (fileName) {
            // var fileName = JSON.parse(fileName);
            fileName = fileName.Result[0].FileURL;
            console.log(fileName.split('/')[4]);
            $$('#specification-attachment-qid').val(fileName.split('/')[4]);
        })
        let file1 = {
            // "FileGUID": JSON.parse(data).Result[0].AttachmentID,
            "FileGUID": (data).Result[0].AttachmentID,
            "FileType": "Builder Certificate"
        }
        getAttachment(file1, function (fileName) {
            // var fileName = JSON.parse(fileName);
            fileName = fileName.Result[0].FileURL;
            console.log(fileName.split('/')[4]);
            $$('#specification-attachment-builder').val(fileName.split('/')[4]);
        })
        let file2 = {
            // "FileGUID": JSON.parse(data).Result[0].AttachmentID,
            "FileGUID": data.Result[0].AttachmentID,
            "FileType": "Bill of Sale"
        }
        getAttachment(file2, function (fileName) {
            // var fileName = JSON.parse(fileName);
            fileName = fileName.Result[0].FileURL;
            console.log(fileName.split('/')[4]);
            $$('#specification-attachment-bill').val(fileName.split('/')[4]);
        })
        let file3 = {
            // "FileGUID": JSON.parse(data).Result[0].AttachmentID,
            "FileGUID": data.Result[0].AttachmentID,
            "FileType": "Deletion Certificate"
        }
        getAttachment(file3, function (fileName) {
            // var fileName = JSON.parse(fileName);
            fileName = fileName.Result[0].FileURL;
            console.log(fileName.split('/')[4]);
            $$('#specification-attachment-deletion').val(fileName.split('/')[4]);
        })
        let file4 = {
            // "FileGUID": JSON.parse(data).Result[0].AttachmentID,
            "FileGUID": data.Result[0].AttachmentID,
            "FileType": "Previous Flag registration Certificate"
        }
        getAttachment(file4, function (fileName) {
            // var fileName = JSON.parse(fileName);
            fileName = fileName.Result[0].FileURL;
            console.log(fileName.split('/')[4]);
            $$('#specification-attachment-flag').val(fileName.split('/')[4]);
        })

    })

    $$('#commercial-registration-attach-container').hide();
    $$('#specification-attachment-input-qid').prop('disabled', true);
    $$('#specification-attachment-input-builder').prop('disabled', true);
    $$('#specification-attachment-input-bill').prop('disabled', true);
    $$('#specification-attachment-input-deletion').prop('disabled', true);
    $$('#specification-attachment-input-flag').prop('disabled', true);
    $$('#request-status-container').hide();
    $$('#comments-section').hide();
    $$('#cancel').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getSpecificationAmendDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner || data.Result[0].Comments;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }


                //data = data.body.d;
                attachmentsGuid = data.Result[0].AttachmentID;
                specificationAttachments(data.Result[0].AttachmentID);

                setTimeout(function () {
                    $$('#ShipAgencyVesselNameTextbox').val(data.Result[0].NewVasselName);
                    $$('#vesselGT').val(data.Result[0].GT);
                    $$('#vesselNT').val(data.Result[0].NT);
                    $$('#vesselBreadth').val(data.Result[0].Breadth);
                    $$('#vesselDepth').val(data.Result[0].Depth);
                    $$('#engineDescription').val(data.Result[0].EngineDescription);
                    $$('#engineTot').val(data.Result[0].NumberofEngines);
                    $$('#engineBHP').val(data.Result[0].BHP);
                    $$('#engineCylinders').val(data.Result[0].NoOfCylinders);
                    $$('#engineShafts').val(data.Result[0].NoOfShafts);
                    $$('#engineMaker').val(data.Result[0].Maker);
                }, 1000)

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-specification').hide();
                    $$('#clear').hide();
                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            $$('#submit-specification').hide();
            $$('#clear').hide();
        }
        app.accordion.open($$(".page-current .accordion-item")[0]);

    }, 1000)


    $$(document).on('click', '#submit-specification', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.NewVesselName = $$('#ShipAgencyVesselNameTextbox').val();
        payload.GT = $$('#vesselGT').val();
        payload.NT = $$('#vesselNT').val();
        payload.Breadth = $$('#vesselBreadth').val();
        payload.Depth = $$('#vesselDepth').val();
        payload.AttachmentId = attachmentsGuid;

        if (typeOfVessel !== 'barge') {
            payload.EngineDescription = $$('#engineDescription').val();
            payload.NumberofEngines = $$('#engineTot').val();
            payload.BHP = $$('#engineBHP').val();
            payload.NoOfCylinders = $$('#engineCylinders').val();
            payload.NoOfShafts = $$('#engineShafts').val();
            payload.Maker = $$('#engineMaker').val();

        }
        console.log(payload);


        attachments.letter = $$('#specification-attachment-letter').val();
        attachments.commercial = $$('#specification-attachment-commercial').val();
        attachments.bank = $$('#specification-attachment-bank').val();
        attachments.flag = $$('#specification-attachment-flag').val();
        attachments.classification = $$('#specification-attachment-classification').val();
        attachments.tonnage = $$('#specification-attachment-tonnage').val();
        attachments.loadLine = $$('#specification-attachment-loadLine').val();
        attachments.cargoRadio = $$('#specification-attachment-cargoRadio').val();
        attachments.compliance = $$('#specification-attachment-compliance').val();
        attachments.safety = $$('#specification-attachment-safety').val();
        attachments.cargoConstruction = $$('#specification-attachment-cargoConstruction').val();
        attachments.cargoSafety = $$('#specification-attachment-cargoSafety').val();
        attachments.oilPollution = $$('#specification-attachment-oilPollution').val();
        attachments.sewagePollution = $$('#specification-attachment-sewagePollution').val();
        attachments.airPollution = $$('#specification-attachment-airPollution').val();

        // if (!payload.NewVesselName || !payload.GT || !payload.NT || !payload.Breadth || !payload.Depth || !payload.EngineDescription || !payload.NumberofEngines || !payload.BHP || !payload.NoOfCylinders || !payload.NoOfShafts || !payload.Maker) {
        if (!payload.NewVesselName || !payload.GT || !payload.NT || !payload.Breadth || !payload.Depth) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        if (typeOfVessel !== 'barge') {
            if (!payload.EngineDescription || !payload.NumberofEngines || !payload.BHP || !payload.NoOfCylinders || !payload.NoOfShafts || !payload.Maker) {
                app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                return;
            }
        }

        if (!attachments.letter ||
            !attachments.classification || !attachments.tonnage || !attachments.loadLine) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (typeOfVessel !== 'barge') {
            if (
                !attachments.cargoRadio || !attachments.compliance || !attachments.safety || !attachments.cargoConstruction ||
                !attachments.cargoSafety || !attachments.oilPollution || !attachments.sewagePollution || !attachments.airPollution) {
                app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
                return;
            }
        }

        $$('#pollution').hide();
        $$('#sewage').hide();
        $$('#oil').hide();
        $$('#cargo-equip').hide();
        $$('#cargo-construction').hide();
        $$('#safety').hide();
        $$('#compliance').hide();
        $$('#cargo').hide();

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateSpecificationAmendDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {
                postSpecificationAmendRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }
        }


    })

    $$(document).on('click', '#cancel-spec', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateSpecificationAmendDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }

    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// Ownership Transfer Page
$$(document).on('page:init', '.page[data-name="ownership-transfer"]', function (e) {

    setLanguage();

    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);

        // if (JSON.parse(data).Result[0].TypeofVessel === 'barge') {
        if (data.Result[0].TypeofVessel === 'barge') {
            $$('#engineSpecsInner').hide();
            $$('#engineSpecsTextboxInner').hide();
        }

        setTimeout(function () {
            // $$('#ShipAgencyVesselNameTextbox').val(JSON.parse(data).Result[0].Name);
            $$('#ShipAgencyVesselNameTextbox').val((data).Result[0].Name);
        }, 2000);

    })

    getAllOwners(function (ownersList) {
        console.log(ownersList);
        let OwnersList = JSON.parse(ownersList);

        var buyers = buyers_TPL(OwnersList);

        var t = e.detail.$el[0];

        $$(t).find(".buyers_details").html(buyers);

        setTimeout(function () {
            $$('#buyerNew').show();
            $$('#buyerExisting').hide();
        }, 1000);

    })



    $$('#request-status-container').hide();
    $$('#qid-buyer-attachement').hide();
    $$('#buyerNew').show();
    $$('#buyerExisting').hide();
    //$$('#commercial-reg-buyer-attachement').hide();
    $$('#cancel').hide();
    $$('#payment-btn').hide();
    $$('#comments-section').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getOwnershipTransferDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }


                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval' ||
                        data.Result[0].RequestStatus === 'PendingOnBuyerApproval') {
                        $$('#cancel').show();
                    }
                }

                getOwner({
                    itemID: data.Result[0].NewOwnerNameId
                }, function (ownerData) {
                    // var ownerData = JSON.parse(ownerData);
                    var ownerData = (ownerData);
                    console.log(ownerData);


                    setTimeout(function () {
                        $$("input[name='owner'][value='Existing']").prop('checked', true);
                        $$('#buyerNew').hide();
                        $$('#buyerExisting').show();
                    }, 1000);

                    if (ownerData.Result[0].OwnerType === 'Company') {
                        $$("input[name='ownerType'][value='Company']").prop('checked', true);
                        $$('#commercial-reg-seller-attachement').show();
                        $$('#qid-seller-attachement').hide();
                    } else if (ownerData.Result[0].OwnerType === 'Individual') {
                        $$("input[name='ownerType'][value='Individual']").prop('checked', true);
                        $$('#qid-seller-attachement').show();
                        $$('#commercial-reg-seller-attachement').hide();
                    }

                    $$('#SelectedOwnerId').val(ownerData.Result[0].ID.toString());
                    // $$('#name').val(ownerData.Result[0].FullName);
                    // $$('#mobile').val(ownerData.Result[0].Mobile);
                    // $$('#email').val(ownerData.Result[0].Email);
                    // $$('#address').val(ownerData.Result[0].Address);
                    // $$('#contact').val(ownerData.Result[0].ContactPerson);
                    // $$('#cr').val(ownerData.Result[0].CR);
                    // $$('#qid').val(ownerData.Result[0].QID);

                    if ((ownerData.Result[0].OwnerType).toLowerCase() === 'company') {
                        $$('#qid-buyer-attachement').hide();
                        $$('#qid-seller-attachement').hide();
                        $$('#commercial-reg-buyer-attachement').show();
                        $$('#commercial-reg-seller-attachement').show();
                        $$('#transfer-qid-container').hide();
                        $$('#transfer-cr-container').show();
                        $$('#transfer-contact-container').show();
                        $$("input[name='ownerType'][value='Company']").prop('checked', true);

                    } else {
                        $$('#commercial-reg-buyer-attachement').hide();
                        $$('#commercial-reg-seller-attachement').hide();
                        $$('#qid-buyer-attachement').show();
                        $$('#qid-seller-attachement').show();
                        $$('#transfer-qid-container').show();
                        $$('#transfer-cr-container').hide();
                        $$('#transfer-contact-container').hide();
                        $$("input[name='ownerType'][value='Individual']").prop('checked', true);
                    }
                })
                $$('#ShipAgencyVesselNameTextbox').val(data.Result[0].Name);
                attachmentsGuid = data.Result[0].AttachmentID;
                transferAttachments(data.Result[0].AttachmentID);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-transfer').hide();
                    $$('#clear').hide();
                    // $$("input[name='ownerType'][value='Company']").prop('disabled', true);
                }

            })
        }

        console.log(localStorage.getItem('isEdit'));
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-transfer').hide();
            $$('#clear').hide();
            $$("input[type=radio]").attr('disabled', true);
            $$('#company-radio').prop('disabled', true);
        }
    }, 1000)

    $$(document).on('click', '#submit-transfer', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var addOwnerPayload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.AttachmentId = attachmentsGuid;
        payload.NewVesselName = $$('#ShipAgencyVesselNameTextbox').val();

        if (ownerNewOrExistingBuyers === 'Existing') {

        } else {

            addOwnerPayload.OwnerType = $$("input[name='ownerType']:checked").val();
            addOwnerPayload.FullName = $$('#name').val();
            addOwnerPayload.MobileNumber = $$('#mobile').val();
            addOwnerPayload.EmailAddress = $$('#email').val();
            addOwnerPayload.Address = $$('#address').val();

            if (addOwnerPayload.OwnerType === 'Company') {
                addOwnerPayload.CR = $$('#cr').val();
                addOwnerPayload.ContactPerson = $$('#contact').val();

            }
            if (addOwnerPayload.OwnerType === 'Individual') {
                addOwnerPayload.QID = $$('#qid').val();
            }

            if (addOwnerPayload.OwnerType === 'Company' && (!addOwnerPayload.CR || !addOwnerPayload.FullName || !addOwnerPayload.ContactPerson || !addOwnerPayload.MobileNumber || !addOwnerPayload.Address || !addOwnerPayload.EmailAddress)) {
                app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                return;
            } else if (addOwnerPayload.OwnerType === 'Individual' && (!addOwnerPayload.QID || !addOwnerPayload.FullName || !addOwnerPayload.MobileNumber || !addOwnerPayload.Address || !addOwnerPayload.EmailAddress)) {
                app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                return;
            }

        }


        console.log(ownerNewOrExistingBuyers);

        attachments.letter = $$('#transfer-attachment-letter').val();
        attachments.certificate = $$('#transfer-attachment-certificate').val();
        attachments.qidBuyer = $$('#transfer-attachment-qidBuyer').val();
        attachments.commercialBuyer = $$('#transfer-attachment-commercialBuyer').val();
        attachments.bill = $$('#transfer-attachment-bill').val();



        // if (!attachments.letter || !attachments.certificate || !attachments.qidBuyer || !attachments.commercialBuyer || !attachments.bill) {

        if (!attachments.letter || !attachments.certificate || !attachments.bill) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateOwnershipTransferDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {

                if (ownerNewOrExistingBuyers === 'Existing') {

                    payload.NewOwnerID = $$('#SelectedOwnerId').val();
                    postOwnershipTransferDetails(payload, function (data) {


                        if (data.statusCode === 201) {
                            showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                            homeView.router.navigate("/home-bgv/");
                        }
                    })

                } else {
                    addOwner(addOwnerPayload, function (data) {


                        // if (data.statusCode === 201) {
                        //   showToast('Data has been submitted successfully');
                        //   homeView.router.navigate("/");
                        // }
                        payload.NewOwnerID = data.Result[0].ID;
                        postOwnershipTransferDetails(payload, function (data) {


                            if (data.statusCode === 201) {
                                showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                                homeView.router.navigate("/home-bgv/");
                            }
                        })
                    })

                }



            }

        }



    })

    $$(document).on('click', '#cancel-trans', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateOwnershipTransferDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }

    })


    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// Ship Deletion Page
$$(document).on('page:init', '.page[data-name="vessel-deletion"]', function (e) {

    setLanguage();

    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);

    })



    $$('#request-status-container').hide();
    $$('#bill-of-sale-attachment-container').hide();
    $$('#evidance-sink-attachment-container').show();
    $$('#bill-of-sale-attachment-container').hide();
    $$('#letter-new-flag').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getShipDeletionDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                //$$('#evidance-sink-attachment-container').hide();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;


                if (data.Result[0].CauseOfDeletion === 'Sold for Foreign Owner') {
                    $$('#evidance-sink-attachment-container').hide();
                    $$('#bill-of-sale-attachment-container').show();
                    $$('#letter-new-flag').show();
                } else if (data.Result[0].CauseOfDeletion === 'Scrap') {
                    $$('#evidance-sink-attachment-container').show();
                    $$('#letter-new-flag').hide();
                    $$('#bill-of-sale-attachment-container').hide();

                } else {
                    $$('#evidance-sink-attachment-container').show();
                    $$('#bill-of-sale-attachment-container').hide();
                    $$('#letter-new-flag').hide();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                //data = data.body.d;
                attachmentsGuid = data.Result[0].AttachmentID;
                deletionAttachments(data.Result[0].AttachmentID);

                $$('#CauseOfDeletion').val(data.Result[0].CauseOfDeletion);
                $$('#CommentsForDeletion').val(data.Result[0].CommentsForDeletion);

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-deletion').hide();
                    $$('#clear').hide();
                    $$('#CauseOfDeletion').prop('disabled', true);
                }

            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            $$('#CauseOfDeletion').prop('disabled', true);
            $$('#submit-deletion').hide();
            $$('#clear').hide();

        }


        app.accordion.open($$(".page-current .accordion-item")[0]);

    }, 1000)


    $$(document).on('click', '#submit-deletion', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.Title = 'Ship Deleton Added';
        payload.CauseOfDeletion = $$('#CauseOfDeletion').val();
        payload.CommentsForDeletion = $$('#CommentsForDeletion').val();
        payload.AttachmentId = attachmentsGuid;
        console.log(payload);

        attachments.letter = $$('#deletion-attachment-letter').val();
        attachments.certificate = $$('#deletion-attachment-certificate').val();
        attachments.flag = $$('#deletion-attachment-flag').val();
        attachments.bill = $$('#deletion-attachment-bill').val();
        attachments.evidence = $$('#deletion-attachment-evidence').val();

        $$('#bill-of-sale-attachment-container').hide();
        $$('#letter-new-flag').hide();

        if (!payload.CauseOfDeletion) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        // if (!attachments.letter || !attachments.certificate || !attachments.flag || !attachments.bill || !attachments.evidence) {
        //   app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
        //   return;
        // }

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');



                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateShipDeletionDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {
                postShipDeletionRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }
        }

    })

    $$(document).on('click', '#cancel', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateShipDeletionDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }


    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// To Whom Concern Page
$$(document).on('page:init', '.page[data-name="to-whom-concern"]', function (e) {

    setLanguage();


    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);
    })

    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();

    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getToWhomDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                attachmentsGuid = data.Result[0].AttachmentID;
                toWhomAttachments(data.Result[0].AttachmentID);
                console.log(data.Result[0]);
                $$('#CertificatePresentedTo').val(data.Result[0].CertificatePresentedTo);
                $$("input[name='IsMortgage'][value=" + data.Result[0].IsMortgage + "]").prop('checked', true);
                $$('#CertificateReasons').val(data.Result[0].CertificateReasons);
                $$('#MortgageSide').val(data.Result[0].MortgageSide);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-whom').hide();
                    $$('#clear').hide();
                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-whom').hide();
            $$('#clear').hide();
        }
        app.accordion.open($$(".page-current .accordion-item")[0]);

    }, 1000)

    $$(document).on('click', '#submit-whom', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.CertificatePresentedTo = $$('#CertificatePresentedTo').val();
        payload.CertificateReasons = $$('#CertificateReasons').val();
        payload.MortgageSide = $$('#MortgageSide').val();
        payload.IsMortrage = $$("input[name='IsMortgage']:checked").val();
        payload.IsMortrage = payload.IsMortrage === 'true' ? '1' : '0';
        payload.AttachmentId = attachmentsGuid;

        console.log(payload);

        attachments.letter = $$('#whom-attachment-letter').val();
        attachments.certificate = $$('#whom-attachment-certificate').val();

        if (!payload.CertificatePresentedTo || !payload.CertificateReasons) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        if (!attachments.letter || !attachments.certificate) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateToWhomConcernRequest(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {
                postToWhomConcernRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }
        }


    })

    $$(document).on('click', '#cancel-whom', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateToWhomConcernRequest(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }


    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

var insurerData = {};
// Safety Insurance
$$(document).on('page:init', '.page[data-name="safety-insurance"]', function (e) {
    insurerEvent = e;
    insurerArray = [];
    setLanguage();

    var insurerList = [];
    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    //getPorts(function (portsList) {
    getAllInsurers(function (insurer) {
        getAllInsuranceType(function (type) {
            getVesselDetails(vesselId, function (data) {
                //
                console.log(type);


                // let ports = JSON.parse(portsList);
                insurerList = JSON.parse(insurer);
                // insurerList = (insurer);
                var insuranceType = JSON.parse(type);
                // var insuranceType = (type);
                //data.Result[0].ports = ports.Result;
                data.Result[0].insurer = insurerList.Result;
                data.Result[0].insuranceType = insuranceType.Result;
                insurerData.insurer = insurerList.Result;

                var insurerDetails = insurer_TPL({ insurer: insurerList.Result });

                var t = insurerEvent.detail.$el[0];

                $$(t).find(".insurer_details").html(insurerDetails);

                // var data = JSON.stringify(data);
                setupVesselInfo(data, e);

                setTimeout(function () {
                    $$('#insurerAddressTextBox').val(insurerList.Result[0].Address);
                }, 3000);

            })


        })
    })

    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();
    setOwnerDetails(e);
    //})

    setTimeout(function () {
        console.log(isEdit);
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getSafetyInsuranceDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);

                insuranceDetailsGetItemById(data.Result[0].ID, function (insuranceDetailsData) {
                    // console.log(JSON.parse(insuranceDetailsData));
                    // let insuranceId = JSON.parse(insuranceDetailsData).Result[0].InsurerId;
                    let insuranceId = (insuranceDetailsData).Result[0].InsurerId;
                    insuranceDetailsData.Result.forEach(function (v, i) {
                        console.log(v);
                        let insuerDetails = {};
                        insurerList.Result.forEach(function (list, i) {
                            if (list.ID === v.InsurerId) {
                                insuerDetails.Name = list.Name;
                                insuerDetails.Address = list.Address;
                            }
                        })
                        insurerArray.push(insuerDetails);
                    })
                    insurerData.insurerArray = insurerArray;
                    setTimeout(function () {
                        let insurerDetailss = insurer_TPL(insurerData);
                        var t = e.detail.$el[0];
                        $$(t).find(".insurer_details").html(insurerDetailss);
                    }, 1000)
                })

                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'FixInspectionProblemsByFlagInspectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }


                attachmentsGuid = data.Result[0].AttachmentID;
                setSafetyInsuranceAttachments(data.Result[0].AttachmentID);

                $$('#SecurityDurationFrom').val(moment(data.Result[0].SecurityDurationFrom).format('YYYY-MM-DD'));
                $$('#SecurityDurationTo').val(moment(data.Result[0].SecurityDurationTo).format('YYYY-MM-DD'));
                $$('#insurance-cert-expiry').val(moment(data.Result[0].ExpiryDate).format('YYYY-MM-DD'));
                $$('#SecurityType').val(data.Result[0].SecurityTypeId.toString());

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-insurance').hide();
                    $$('#clear').hide();
                    $$('#SecurityType').prop('disabled', true);
                    $$('#insurerName').prop('disabled', true);
                    setTimeout(function () {
                        $$('#insurer-form').hide();
                        $$('#insurer-add-btn').hide();
                    }, 3000)
                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-insurance').hide();
            $$('#clear').hide();
            setTimeout(function () {
                $$('#insurer-form').hide();
                $$('#insurer-add-btn').hide();
            }, 3000)
        }
        app.accordion.open($$(".page-current .accordion-item")[0]);

    }, 1000)

    $$(document).on('click', '#submit-insurance', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.SecurityDurationFrom = moment($$('#SecurityDurationFrom').val()).format('MM/DD/YYYY');
        payload.SecurityDurationTo = moment($$('#SecurityDurationTo').val()).format('MM/DD/YYYY');
        payload.SecurityType = $$('#SecurityType').val();
        payload.AttachmentId = attachmentsGuid;

        // if($$('SecurityType').val() === 'security1') {
        //   payload.SecurityType = '1';
        // } 
        // else if($$('SecurityType').val() === 'security2') {
        //   payload.SecurityType = '2';
        // }

        console.log(payload.SecurityDurationTo);


        attachments.letter = $$('#insurance-attachment-letter').val();
        attachments.policy = $$('#insurance-attachment-policy').val();

        if (insurerArray.length === 0) {
            app.dialog.alert('Please add atleast one insurer', 'Alert');
            return;
        }

        if (payload.SecurityDurationFrom === 'Invalid date' || payload.SecurityDurationTo === 'Invalid date' || !payload.SecurityType) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        if (!attachments.letter || !attachments.policy) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateSafetyInsuranceDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        let insuranceUpdateAddressPayload = {
                            itemID: $$('#insurerName').val(),
                            Address: $$('#insurerAddressTextBox').val()
                        }

                        insuranceUpdateAddress(insuranceUpdateAddressPayload, function (insuranceUpdateData) {
                            // if (JSON.parse(insuranceUpdateData).statusCode === 204) {
                            if ((insuranceUpdateData).statusCode === 204) {
                                showToast('Request ID ' + localStorage.getItem('itemId') + ' has submitted successfully');
                                homeView.router.navigate("/home-bgv/");
                            }
                        })

                    }
                })
            } else {
                postSafetyInsuranceRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        insurerArray.forEach(function (v, i) {
                            let insuranceDetailsPayload = {
                                Insurer: v.ID,
                                SafetyInsurance: data.Result[0].ID
                            }
                            insuranceDetailsAddItem(insuranceDetailsPayload, function (insuranceDetailsData) {
                                // if (JSON.parse(insuranceDetailsData).statusCode === 201) {
                                if ((insuranceDetailsData).statusCode === 201) {
                                    let insuranceUpdateAddressPayload = {
                                        itemID: v.ID,
                                        Address: v.Address
                                    }
                                    insuranceUpdateAddress(insuranceUpdateAddressPayload, function (insuranceUpdateData) {
                                        // if (JSON.parse(insuranceUpdateData).statusCode === 204) {
                                        if ((insuranceUpdateData).statusCode === 204) {
                                            showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                                            homeView.router.navigate("/home-bgv/");
                                        }
                                    })
                                }
                            })
                        })
                    }
                })
            }
        }
    })

    $$(document).on('click', '#cancel-safety', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateSafetyInsuranceDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }

    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// Extension Certificate
$$(document).on('page:init', '.page[data-name="extension-certificate"]', function (e) {

    setLanguage();

    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getPorts(function (portsList) {
        getVesselDetails(vesselId, function (data) {

            // let ports = JSON.parse(portsList);
            let ports = (portsList);
            data.Result[0].ports = ports.Result;
            setupVesselInfo(data, e);
        })
        setOwnerDetails(e);
        setTimeout(() => {

            app.accordion.open($$(".page-current .accordion-item")[0]);


        }, 200);
    })

    $$('#comments-section').hide();
    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();

    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getExtensionDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;


                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;


                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'FixInspectionProblemsByFlagInspectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }


                attachmentsGuid = data.Result[0].AttachmentID;
                setExtensionAttachments(data.Result[0].AttachmentID);

                $$('#Regulations').val(data.Result[0].Regulations);
                $$("input[value=" + data.Result[0].RequestType + "]").prop('checked', true);
                $$('#OtherMeasures').val(data.Result[0].OtherMeasures);
                $$('#AllRequestsExpiration').val(data.Result[0].AllRequestsExpiration);

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-authorization').hide();
                    $$('#clear').hide();
                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-authorization').hide();
            $$('#clear').hide();
        }
    }, 1000)

    $$(document).on('click', '#submit-authorization', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.RequestType = $$("input[name='RequestType']:checked").val();
        payload.Regulations = $$('#Regulations').val().trim();
        payload.OtherMeasures = $$('#OtherMeasures').val().trim();
        payload.AllRequestsExpiration = $$('#AllRequestsExpiration').val().trim();
        payload.AttachmentId = attachmentsGuid;
        console.log(payload);

        attachments.letter = $$('#authorization-attachment-letter').val();
        attachments.class = $$('#authorization-attachment-class').val();

        if (!payload.Regulations) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        if (!attachments.letter || !attachments.class) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'FixInspectionProblemsByFlagInspectionEmployee') {
                    payload.RequestStatus = 'PendingOnFlagInspectionEmployeeApproval';
                }

                updateExtensionDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {
                postCertificateExtensionRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }


        }

    })

    $$(document).on('click', '#cancel-extension', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateExtensionDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }


    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// NOL Certificate
$$(document).on('page:init', '.page[data-name="nol-certificate"]', function (e) {
    nolMarineEvent = e;
    setLanguage();


    let nolRequestId = '';
    var disableCheckbox = false;
    isFile = false;
    natureOfOperationArr = [];
    nationalityArrayUpdate = [];
    nationalityArray = [];
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);
    })

    var marineDetailsData = ''
    var marineDetails = marine_TPL(marineDetailsData);


    var t = e.detail.$el[0];

    $$(t).find(".marine-craft-details").html(marineDetails);


    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();

    $$('#CommencedDate').val(moment().format('YYYY-MM-DD'));
    $$('#CompletionDate').val(moment().format('YYYY-MM-DD'));

    $$('#duration-of-work')[0].innerHTML = 1;

    $$('.operationsCheckbox').on('click', function (e) {
        if (disableCheckbox) {
            e.preventDefault();
        }

    });


    setTimeout(function () {
        if (isEdit) {

            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getNOLDetails(request.id, function (data) {


                nolRequestGetNationality({
                    itemID: request.id
                }, function (nationality) {

                    // $$('#marineCraftName').val(JSON.parse(nationality).Result[0].Name);
                    // $$('#marineCraftNationality').val(JSON.parse(nationality).Result[0].Nationality);
                    nationalityArray = (nationality).Result
                    var marineDetails = marine_TPL({
                        nationalityArray: (nationality).Result
                    });
                    var t = e.detail.$el[0];
                    $$(t).find(".marine-craft-details").html(marineDetails);

                    natureOfOperationGetItemByID({
                        itemID: request.id
                    }, function (operations) {
                        // let nolNatureofOperations = JSON.parse(operations).Result;
                        let nolNatureofOperations = (operations).Result;
                        nolNatureofOperations.forEach(function (v, i) {
                            natureOfOperationArr.push(v.NOLNatureOfOperationId)
                            if (v.NOLNatureOfOperationId === 1) {
                                $$('#towing').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 2) {
                                $$('#marine-shooting').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 3) {
                                $$('#exploration-drilling').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 4) {
                                $$('#maritime-survey').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 5) {
                                $$('#construction').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 6) {
                                $$('#maintenance').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 7) {
                                $$('#wreckage-recovered').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 8) {
                                $$('#driving').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 9) {
                                $$('#cable-lying').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 10) {
                                $$('#cable-repair').prop('checked', true);
                            } else if (v.NOLNatureOfOperationId === 11) {
                                $$('#other').prop('checked', true);
                            }
                        })
                    })

                })


                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;
                nolRequestId = data.Result[0].ID;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                attachmentsGuid = data.Result[0].AttachmentID;
                nolAttachments(data.Result[0].AttachmentID);
                console.log(data.Result[0]);
                $$('#CommencedDate').val(moment(data.Result[0].CommencedDate).format('YYYY-MM-DD'));
                $$('#CompletionDate').val(moment(data.Result[0].CompletionDate).format('YYYY-MM-DD'));
                $$('#Longitude').val(data.Result[0].Longitude);
                $$('#Latitude').val(data.Result[0].Latitude);
                $$('#ShipAgency').val(data.Result[0].ShipAgency);
                $$('#OperatingCompany').val(data.Result[0].OperatingCompany);
                $$('#NumOfMarineCrafts').val(Number(data.Result[0].NumOfMarineCrafts));
                $$('#RequestingParty').val(data.Result[0].RequestingParty);
                $$('#PersonInCharge').val(data.Result[0].PersonInCharge);
                $$('#CompanyNumber').val(data.Result[0].CompanyNumber);
                $$('#Mobile').val(data.Result[0].Mobile);
                $$('#Email').val(data.Result[0].Email);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    setTimeout(function () {
                        $$('input').prop('disabled', true);
                        $$('.operationsCheckbox').prop('disabled', false);
                        $$('textarea').prop('disabled', true);
                        $$('#submit-nol').hide();
                        $$('#clear').hide();
                        $$('#nationality-form').hide();
                        $$('#add-btn').hide();
                        disableCheckbox = true;
                    }, 1000)

                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            setTimeout(function () {
                $$('input').attr('prop', true);
                $$('.operationsCheckbox').prop('disabled', false);
                $$('textarea').prop('disabled', true);
                //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
                $$('#submit-nol').hide();
                $$('#clear').hide();
                $$('#nationality-form').hide();
                $$('#add-btn').hide();
                disableCheckbox = true;
            }, 1000)
        }
    }, 1000)

    $$(document).on('click', '#submit-nol', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.CommencedDate = moment($$('#CommencedDate').val()).format('MM/DD/YYYY');
        payload.CompletionDate = moment($$('#CompletionDate').val()).format('MM/DD/YYYY');
        payload.NumOfMarineCrafts = $$('#NumOfMarineCrafts').val();
        payload.OperatingCompany = $$('#OperatingCompany').val();
        payload.ShipAgency = $$('#ShipAgency').val();
        payload.Latitude = $$('#Latitude').val();
        payload.Longitude = $$('#Longitude').val();
        payload.RequestingParty = $$('#RequestingParty').val();
        payload.PersonInCharge = $$('#PersonInCharge').val();
        payload.Mobile = $$('#Mobile').val();
        payload.Email = $$('#Email').val();
        payload.CompanyNumber = $$('#CompanyNumber').val();
        payload.AttachmentId = attachmentsGuid;
        console.log(payload);

        attachments.letter = $$('#nol-attachment-letter').val();
        attachments.flag = $$('#nol-attachment-flag').val();
        attachments.foreign = $$('#nol-attachment-foreign').val();
        attachments.navigational = $$('#nol-attachment-navigational').val();
        attachments.arabic = $$('#nol-attachment-arabic').val();
        attachments.english = $$('#nol-attachment-english').val();


        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;

            if (!payload.CommencedDate || !payload.CompletionDate || !payload.NumOfMarineCrafts || !payload.OperatingCompany ||
                !payload.ShipAgency || !payload.Latitude || !payload.RequestingParty || !payload.PersonInCharge ||
                !payload.CompanyNumber || !payload.Mobile) {
                app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                return;
            }

            if (payload.CommencedDate === 'Invalid date' || payload.CompletionDate === 'Invalid date') {
                app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                return;
            }

            if (natureOfOperationArr.length === 0) {
                app.dialog.alert('Please select atleast one nature of operation', 'Alert');
                return;
            }

            if (nationalityArray.length === 0) {
                app.dialog.alert('Please add atleast one nationality', 'Alert');
                return;
            }




            // if (!attachments.letter || !attachments.flag || !attachments.foreign || !attachments.navigational || !attachments.arabic || !attachments.english) {
            //   app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            //   return;
            // }

            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateNOLDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        if (nationalityArrayUpdate.length > 0) {

                            nationalityArrayUpdate.forEach(function (v, i) {
                                console.log(v)
                                v.RequestID = nolRequestId.toString();

                                nolRequestAddNationality(v, function (nationality) {
                                    if (i === nationalityArray.length - 1) {
                                        deleteNatureOfOperation(nolRequestId.toString(), function (response) {
                                            if (response.statusCode == 200) {
                                                natureOfOperationArr.forEach(function (v, i) {
                                                    var payload2 = {
                                                        'NOLNatureOfOperation': v.toString(),
                                                        'RequestID': nolRequestId.toString()
                                                    }
                                                    nolRequestAddNatureOfOperation(payload2, function (operation) {
                                                        if (i + 1 === natureOfOperationArr.length) {
                                                            showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                                                            homeView.router.navigate("/home-bgv/");
                                                        }

                                                    })
                                                })
                                            }
                                        })
                                    }


                                })
                            })

                        }
                        else {
                            deleteNatureOfOperation(nolRequestId.toString(), function (response) {
                                if (response.statusCode == 200) {
                                    natureOfOperationArr.forEach(function (v, i) {
                                        var payload2 = {
                                            'NOLNatureOfOperation': v.toString(),
                                            'RequestID': nolRequestId.toString()
                                        }
                                        nolRequestAddNatureOfOperation(payload2, function (operation) {
                                            if (i + 1 === natureOfOperationArr.length) {
                                                showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                                                homeView.router.navigate("/home-bgv/");
                                            }

                                        })
                                    })
                                }
                            })
                        }

                    }

                })
            } else {
                postNOLCertificateRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        // var payload1 = {
                        //   Name: $$('#marineCraftName').val(),
                        //   Nationality: $$('#marineCraftNationality').val(),
                        //   RequestID: data.Result[0].ID
                        // }
                        nationalityArray.forEach(function (v, i) {
                            console.log(v)
                            v.RequestID = data.Result[0].ID

                            nolRequestAddNationality(v, function (nationality) {
                                if (i === nationalityArray.length - 1)
                                    natureOfOperationArr.forEach(function (v, i) {
                                        var payload2 = {
                                            'NOLNatureOfOperation': v,
                                            'RequestID': data.Result[0].ID
                                        }
                                        nolRequestAddNatureOfOperation(payload2, function (operation) {
                                            if (i + 1 === natureOfOperationArr.length) {
                                                showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                                                homeView.router.navigate("/home-bgv/");
                                            }

                                        })
                                    })

                            })
                        })





                    }
                })
            }

        }



    })

    $$(document).on('click', '#cancel-nol', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateNOLDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }

    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});
var replacementRequestStatus = '';
// Lost Replacement Certificate
$$(document).on('page:init', '.page[data-name="lostreplacement-certificate"]', function (e) {
    replacementRequestStatus = 'New';
    setLanguage();

    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    getVesselDetails(vesselId, function (data) {
        // console.log(JSON.parse(data).Result[0]);
        // let vesselStatus = JSON.parse(data).Result[0].Vesselstatus;
        let vesselStatus = (data).Result[0].Vesselstatus;
        if (vesselStatus === 'Permanent') {
            $$('#provisional').hide();
        } else if (vesselStatus === 'Provisional') {
            $$('#permanent').hide();
        }
        if (vesselStatus !== 'Provisional') {
            setTimeout(function () {
                $$('#expiry-date').hide();
            }, 100)
        }
        setupVesselInfo(data, e);
        setOwnerDetails(e);
        app.accordion.open($$(".page-current .accordion-item")[0]);


    })

    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getLostAndReplacementDetails(request.id, function (data) {


                localStorage.setItem('itemId', data.Result[0].ID);
                replacementRequestStatus = data.Result[0].RequestStatus;
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;
                $$('#serviceFee')[0].innerHTML = data.Result[0].CertificateFee.toString();

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    //$$('#payment-btn').show();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                attachmentsGuid = data.Result[0].AttachmentID;
                lostAttachments(data.Result[0].AttachmentID);
                $$('#RequestReasons').val(data.Result[0].RequestReasons);

                data.Result[0].CertificateTypeId = Number(data.Result[0].CertificateTypeId);
                if (data.Result[0].CertificateTypeId === 2) {
                    $$("input[name='CertificateType'][value='Provisional']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {


                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Provisional Registery') {
                                let payload = {
                                    ListGUID: service.ListGuid,
                                    OwnerID: owner.ID,
                                    VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                                }
                                if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                    showLostSubmitBtn('true');
                                }
                                // requestIsExist(payload, function (data) {
                                //   let isExist = JSON.parse(data).isExist;
                                //   if (isExist === 0) {
                                //     showLostSubmitBtn('false');
                                //     app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                //   }
                                //   else {

                                //   }
                                // })
                            }
                        })
                    })
                } else if (data.Result[0].CertificateTypeId === 3) {
                    $$("input[name='CertificateType'][value='Permanent']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {


                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Permenant Registery') {
                                if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                    showLostSubmitBtn('true');
                                }

                                // requestIsExist(payload, function (data) {
                                //   let isExist = JSON.parse(data).isExist;
                                //   if (isExist === 0) {
                                //     showLostSubmitBtn('false');
                                //     app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                //   }
                                //   else {

                                //   }
                                // })
                            }
                        })
                    })
                } else if (data.Result[0].CertificateTypeId === 5) {
                    $$("input[name='CertificateType'][value='Trading']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {


                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Trading Certificate') {
                                let payload = {
                                    ListGUID: service.ListGuid,
                                    OwnerID: owner.ID,
                                    VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                                }
                                requestIsExist(payload, function (data) {

                                    let isExist = (data).isExist;
                                    if (isExist === 0) {
                                        showLostSubmitBtn('false');
                                        app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                    } else {
                                        if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                            data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                            showLostSubmitBtn('true');
                                        }
                                    }
                                })
                            }
                        })
                    })
                } else if (data.Result[0].CertificateTypeId === 15) {
                    $$("input[name='CertificateType'][value='Safety']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {


                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Safety Insurance For Oil Pollution Damage') {
                                let payload = {
                                    ListGUID: service.ListGuid,
                                    OwnerID: owner.ID,
                                    VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                                }
                                requestIsExist(payload, function (data) {

                                    let isExist = (data).isExist;
                                    if (isExist === 0) {
                                        showLostSubmitBtn('false');
                                        app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                    } else {
                                        if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                            data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                            showLostSubmitBtn('true');
                                        }
                                    }
                                })
                            }
                        })
                    })
                } else if (data.Result[0].CertificateTypeId === 9) {
                    $$("input[name='CertificateType'][value='CSR']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {

                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Continuous Synopsis Record') {
                                let payload = {
                                    ListGUID: service.ListGuid,
                                    OwnerID: owner.ID,
                                    VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                                }
                                requestIsExist(payload, function (data) {

                                    let isExist = (data).isExist;
                                    if (isExist === 0) {
                                        showLostSubmitBtn('false');
                                        app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                    } else {
                                        if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                            data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                            showLostSubmitBtn('true');
                                        }
                                    }
                                })
                            }
                        })
                    })
                } else if (data.Result[0].CertificateTypeId === 14) {
                    $$("input[name='CertificateType'][value='Exemption']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {


                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Exemption Certificate') {
                                let payload = {
                                    ListGUID: service.ListGuid,
                                    OwnerID: owner.ID,
                                    VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                                }
                                requestIsExist(payload, function (data) {

                                    let isExist = (data).isExist;
                                    if (isExist === 0) {
                                        showLostSubmitBtn('false');
                                        app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                    } else {
                                        if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                            data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                            showLostSubmitBtn('true');
                                        }
                                    }
                                })
                            }
                        })
                    })
                } else if (data.Result[0].CertificateTypeId === 17) {
                    $$("input[name='CertificateType'][value='Authorization']").prop('checked', true);
                    getCertificateList(function (vesselServicesData) {


                        let services = JSON.parse(vesselServicesData).Result;
                        services.forEach(function (service) {
                            if (service.Name === 'Extension Certificate') {
                                let payload = {
                                    ListGUID: service.ListGuid,
                                    OwnerID: owner.ID,
                                    VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                                }
                                requestIsExist(payload, function (data) {

                                    let isExist = (data).isExist;
                                    if (isExist === 0) {
                                        showLostSubmitBtn('false');
                                        app.dialog.alert('You donot have any request for the selected service', 'Alert');
                                    } else {
                                        if (localStorage.getItem('isEdit') !== 'false' && (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                            data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee')) {
                                            showLostSubmitBtn('true');
                                        }
                                    }
                                })
                            }
                        })
                    })
                }

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-lost').hide();
                    $$('#clear').hide();
                }
            })
        } else {
            getCertificateList(function (vesselServicesData) {

                let services = JSON.parse(vesselServicesData).Result;
                services.forEach(function (service) {
                    if (service.Name === 'Trading Certificate') {
                        let payload = {
                            ListGUID: service.ListGuid,
                            OwnerID: owner.ID,
                            VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                        }
                        requestIsExist(payload, function (data) {

                            let isExist = (data).isExist;
                            if (isExist === 0 && localStorage.getItem('isEdit') !== 'false') {
                                showLostSubmitBtn('false');
                                app.dialog.alert('You donot have any request for the selected service', 'Alert');
                            } else {
                                if (localStorage.getItem('isEdit') !== 'false') {
                                    showLostSubmitBtn('true');
                                }
                            }
                        })
                    }
                })
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-authorization').hide();
            $$('#clear').hide();
        }
    }, 1000)

    $$(document).on('click', '#submit-lost', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.RequestReasons = $$('#RequestReasons').val();
        payload.AttachmentId = attachmentsGuid;
        payload.CertificateFee = $$('#serviceFee')[0].innerHTML;
        if ($$("input[name='CertificateType']:checked").val() === 'Provisional') {
            payload.CertificateType = '2';
        } else if ($$("input[name='CertificateType']:checked").val() === 'Permanent') {
            payload.CertificateType = '3';
        } else if ($$("input[name='CertificateType']:checked").val() === 'Trading') {
            payload.CertificateType = '5';
        } else if ($$("input[name='CertificateType']:checked").val() === 'Safety') {
            payload.CertificateType = '15';
        } else if ($$("input[name='CertificateType']:checked").val() === 'CSR') {
            payload.CertificateType = '9';
        } else if ($$("input[name='CertificateType']:checked").val() === 'Exemption') {
            payload.CertificateType = '14';
        } else if ($$("input[name='CertificateType']:checked").val() === 'Authorization') {
            payload.CertificateType = '17';
        }
        console.log(payload);


        attachments.letter = $$('#lost-attachment-letter').val();
        attachments.certificate = $$('#lost-attachment-certificate').val();

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;

            if (!payload.CertificateType || !payload.RequestReasons) {
                app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                return;
            }

            if (!attachments.letter || !attachments.certificate) {
                app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
                return;
            }


            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateLostAndReplacementDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })

            } else {
                postLostReplacementRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })

            }

        }

    })

    $$(document).on('click', '#cancel-lost', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateLostAndReplacementDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })

        }

    })
    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// Exemption Certificate
$$(document).on('page:init', '.page[data-name="exemption-certificates"]', function (e) {


    setLanguage();



    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getPorts(function (portsList) {
        getVesselDetails(vesselId, function (data) {

            let ports = (portsList);
            data.Result[0].ports = ports.Result;
            setupVesselInfo(data, e);
        })
        setOwnerDetails(e);
    })

    $$('#request-status-container').hide();
    $$('#comments-section').hide();
    $$('#cancel').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {
        if (isEdit && isEdit !== null) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getExemptionDetails(request.id, function (data) {


                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#comments-section').show();
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }


                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                attachmentsGuid = data.Result[0].AttachmentID;

                setExemptionAttachments(data.Result[0].AttachmentID);


                $$('#Regulations').val(data.Result[0].Regulations);
                $$('#ExemptedRequirements').val(data.Result[0].ExemptedRequirements);
                $$('#ConferredByRegulation').val(data.Result[0].ConferredByRegulation);
                $$('#SupportingReasons').val(data.Result[0].SupportingReasons);
                $$('#ExemptionDuration').val(data.Result[0].ExemptionDuration);
                $$('#RegulaRecognizedOrganizationtions').val(data.Result[0].RecognizedOrganization);
                $$('#OtherMeasures').val(data.Result[0].OtherMeasures);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-exemptions').hide();
                    $$('#clear').hide();
                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-exemptions').hide();
            $$('#clear').hide();
        }
    }, 1000)


    $$(document).on('click', '#submit-exemptions', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.Regulations = $$('#Regulations').val();
        payload.ExemptedRequirements = $$('#ExemptedRequirements').val().trim();
        payload.ConferredByRegulation = $$('#ConferredByRegulation').val().trim();
        payload.SupportingReasons = $$('#SupportingReasons').val().trim();
        payload.ExemptionDuration = $$('#ExemptionDuration').val().trim();
        payload.OtherMeasures = $$('#OtherMeasures').val().trim();
        payload.RecognizedOrganization = $$('#RegulaRecognizedOrganizationtions').val().trim();
        payload.AttachmentId = attachmentsGuid;
        console.log(payload);

        attachments.letter = $$('#exemption-attachment-letter').val();
        attachments.class = $$('#exemption-attachment-class').val();
        attachments.subject = $$('#exemption-attachment-subject').val();
        attachments.payment = $$('#exemption-attachment-payment').val();

        if (!payload.Regulations || !payload.ExemptedRequirements || !payload.ConferredByRegulation || !payload.SupportingReasons) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        if (!attachments.letter || !attachments.class) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'FixInspectionProblemsByFlagInspectionEmployee') {
                    payload.RequestStatus = 'PendingOnFlagInspectionEmployeeApproval';
                }
                updateExemptionDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })

            } else {
                postExemptionRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })

            }

        }


    })

    $$(document).on('click', '#cancel-exemption', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateExemptionDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })
        }

    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

    setTimeout(() => {
        if (localStorage.getItem('isEdit') == "null") {

            app.accordion.open($$("div.page.page-current .requestDetails"));

        }

    }, 100);


});

// Derrating Certificate
$$(document).on('page:init', '.page[data-name="derrating-certificate"]', function (e) {

    setLanguage();
    isQatariRegistration = '1';

    isFile = false;
    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    getMyVessels(3, function (vesselsList) {
        getVesselDetails(vesselId, function (data) {


            let vesselList = (vesselsList);
            data.Result[0].vesselsList = vesselList.Result;
            // var data = JSON.stringify(data);
            testData = data;
            setTimeout(function () {
                // $$('#VesselOfficialNumberTextbox').val(JSON.parse(data).Result[0].OfficialNumber);
                $$('#VesselOfficialNumberTextbox').val((data).Result[0].OfficialNumber);
                // $$('#ShipAgencyVesselName').val(JSON.parse(data).Result[0].Name);
                $$('#ShipAgencyVesselName').val((data).Result[0].Name);
                $$('#ShipAgencyVesselNameTextbox').val('');

                $$('#vesselNameTextbox').hide();
                $$('#OfficialNumberTextbox').hide();
                $$('#OfficialNumber').show();

            }, 2000)
            setupVesselInfo(data, e);
            setOwnerDetails(e);

            var t = e.detail.$el[0];
            app.accordion.open($$(t).find(".first-accordian"));
        })

    })

    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();

    app.accordion.open($$(".page-current .accordion-item")[0]);


    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getDerratingDetails(request.id, function (data) {


                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;
                $$('#ShipAgencyVesselName').prop('disabled', true);

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();


                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                } else {
                    if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                        data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                        $$('#commentsToVesselOwnerContainer').show();
                        $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;
                        $$('#ShipAgencyVesselName').prop('disabled', false);

                    }
                    if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                        $$('#comments-section').show();
                        $$('#commentsFromVesselOwnerContainer').show();
                        $$('#flagInspectorContainer').show();
                        $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                        $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                    }
                }
                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                attachmentsGuid = data.Result[0].AttachmentID;
                derratingAttachments(data.Result[0].AttachmentID);

                setTimeout(function () {
                    if (data.Result[0].IsQatariRegistration) {
                        $$("input[name='IsQatariRegistration'][value='1']").prop('checked', true);
                        $$('#ShipAgencyVesselName').val(data.Result[0].ShipAgencyVesselName);
                        $$('#vesselNameTextbox').hide();
                        $$('#showVesselsDropdown').show();
                        $$('#OfficialNumberTextbox').hide();
                        $$('#OfficialNumber').show();
                        // $$('#VesselOfficialNumber')[0].innerHTML = JSON.parse(data).Result[0].OfficialNumber;
                        $$('#VesselOfficialNumber')[0].innerHTML = (data).Result[0].OfficialNumber;
                    } else {
                        $$("input[name='IsQatariRegistration'][value='0']").prop('checked', true);
                        $$('#ShipAgencyVesselNameTextbox').val(data.Result[0].ShipAgencyVesselName);
                        $$('#VesselOfficialNumberTextbox').val(data.Result[0].OfficialNumber);
                        $$('#vesselNameTextbox').show();
                        $$('#showVesselsDropdown').hide();
                        $$('#OfficialNumberTextbox').show();
                        $$('#OfficialNumber').hide();
                    }
                }, 2000)

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    // $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-derating').hide();
                    $$('#clear').hide();
                }

            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            $$('#ShipAgencyVesselName').prop('disabled', true);
            $$('#submit-derating').hide();
            $$('#clear').hide();
            $$('.attachment-delete-icon').hide();
        }
        app.accordion.open($$(".page-current .accordion-item")[0]);

    }, 1000)

    $$(document).on('click', '#submit-derating', function (e) {
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
            var payload = {};
            var attachments = {};
            payload.OwnerID = owner.ID;
            payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
            payload.IsQatariRegistration = isQatariRegistration;
            payload.AttachmentId = attachmentsGuid;
            if (isQatariRegistration) {
                payload.OfficialNumber = $$('#VesselOfficialNumber')[0].innerHTML;
            } else {
                payload.OfficialNumber = $$('#VesselOfficialNumberTextbox').val();
            }



            console.log(payload);

            attachments.letter = $$('#derating-attachment-letter').val();
            attachments.rodent = $$('#derating-attachment-rodent').val();


            if (!attachments.letter || !attachments.rodent) {
                app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
                return;
            }


            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }

                if (payload.IsQatariRegistration === '0') {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselNameTextbox').val();

                    if (!payload.ShipAgencyVesselName) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }

                    updateDerratingDetails(payload, function (data) {


                        if (data.statusCode === 204) {
                            showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                            homeView.router.navigate("/home-bgv/");
                        }
                    })
                } else {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselName').val();
                    if (!payload.ShipAgencyVesselName) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }
                    getMyVessels(3, function (vesselsList) {

                        let vesselList = (vesselsList);
                        vesselList.Result.forEach(function (vessel) {
                            if (vessel.Name === $$('#ShipAgencyVesselName').val()) {
                                payload.VesselID = vessel.ID;
                                updateDerratingDetails(payload, function (data) {


                                    if (data.statusCode === 204) {
                                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                                        homeView.router.navigate("/home-bgv/");
                                    }
                                })
                            }
                        })

                    })
                }

            } else {
                if (payload.IsQatariRegistration === '0') {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselNameTextbox').val();
                    payload.OfficialNumber = $$('#VesselOfficialNumberTextbox').val();
                    if (!payload.ShipAgencyVesselName) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }
                    postDeratingCertificateRequest(payload, function (data) {


                        if (data.statusCode === 201) {
                            showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                            homeView.router.navigate("/home-bgv/");
                        }
                    })
                } else {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselName').val();
                    if (!payload.ShipAgencyVesselName) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }
                    getMyVessels(3, function (vesselsList) {

                        let vesselList = (vesselsList);
                        vesselList.Result.forEach(function (vessel) {
                            if (vessel.Name === $$('#ShipAgencyVesselName').val()) {
                                payload.VesselID = vessel.ID;
                                postDeratingCertificateRequest(payload, function (data) {


                                    if (data.statusCode === 201) {
                                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                                        homeView.router.navigate("/home-bgv/");
                                    }
                                })
                            }
                        })

                    })
                }

            }
        }


    })

    $$(document).on('click', '#cancel-derating', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateDerratingDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })
        }
    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })


});


// Marine Incident Report
$$(document).on('page:init', '.page[data-name="marine-incident"]', function (e) {

    setLanguage();
    isQatariRegistration = '1';

    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    isFile = false;
    getMyVessels(3, function (vesselsList) {
        getVesselDetails(vesselId, function (vesselDetails) {
            // var data = JSON.parse(vesselDetails);
            var data = (vesselDetails);
            // var VesselDetails = JSON.parse(vesselDetails);
            var VesselDetails = (vesselDetails);

            setTimeout(function () {
                $$('#vesselNameTextbox').hide();
                $$('#ShipAgencyVesselName').val(VesselDetails.Result[0].Name);
            }, 2000);

            let vesselList = (vesselsList);
            data.Result[0].vesselsList = vesselList.Result;
            // var data = JSON.stringify(data);
            testData = data;
            setupVesselInfo(data, e);
            setOwnerDetails(e);

        })
    })


    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();


    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getMarineIncidentDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;
                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();

                // $$('#ShipAgencyVesselName').val(data.Result[0].ShipAgencyVesselName);
                setTimeout(function () {
                    $$('#CaptainName').val(data.Result[0].CaptainName);
                    $$('#ShipAgencyName').val(data.Result[0].ShipAgencyName);
                    $$('#ShipAgencyAddress').val(data.Result[0].ShipAgencyAddress);
                    $$('#ShipAgencyMobile').val(data.Result[0].ShipAgencyMobile);
                    $$('#ShipAgencyEmail').val(data.Result[0].ShipAgencyEmail);

                    if (data.Result[0].IsQatariRegistration) {
                        $$("input[name='IsQatariRegistration'][value='1']").prop('checked', true);
                        $$('#ShipAgencyVesselName').val(data.Result[0].ShipAgencyVesselName);
                        $$('#vesselNameTextbox').hide();
                        $$('#showVesselsDropdown').show();
                    } else {
                        $$("input[name='IsQatariRegistration'][value='0']").prop('checked', true);
                        $$('#ShipAgencyVesselNameTextbox').val(data.Result[0].ShipAgencyVesselName);
                        $$('#vesselNameTextbox').show();
                        $$('#showVesselsDropdown').hide();
                    }
                }, 4000)
                $$('#ShipAgencyVesselName').prop('disabled', true);

                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                } else {
                    if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                        data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                        $$('#commentsToVesselOwnerContainer').show();
                        $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;
                        $$('#ShipAgencyVesselName').prop('disabled', false);

                    }
                }
                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                attachmentsGuid = data.Result[0].AttachmentID;
                marineAttachments(data.Result[0].AttachmentID);
                console.log(data.Result[0]);
                //setTimeout(function () {


                //}, 2000);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    setTimeout(function () {
                        $$('input').prop('disabled', true);
                        $$('textarea').prop('disabled', true);
                        $$('#submit-marine').hide();
                        $$('#clear').hide();
                    }, 2000);
                }

            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            setTimeout(function () {
                $$('input').prop('disabled', true);
                $$('textarea').prop('disabled', true);
                $$('#ShipAgencyVesselName').prop('disabled', true);
                $$('#submit-marine').hide();
                $$('#clear').hide();
            }, 2000);
        }



        app.accordion.open($$(".page-current .accordion-item")[0]);



    }, 1000)

    $$(document).on('click', '#submit-marine', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');;
        payload.IsQatariRegistration = isQatariRegistration;
        payload.CaptainName = $$('#CaptainName').val();
        payload.ShipAgencyName = $$('#ShipAgencyName').val();
        payload.ShipAgencyAddress = $$('#ShipAgencyAddress').val();
        payload.ShipAgencyMobile = $$('#ShipAgencyMobile').val();
        payload.ShipAgencyEmail = $$('#ShipAgencyEmail').val();
        payload.AttachmentId = attachmentsGuid;


        console.log(payload);

        attachments.log = $$('#marine-attachment-log').val();
        attachments.report = $$('#marine-attachment-report').val();

        if (!attachments.log || !attachments.report) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }

                if (payload.IsQatariRegistration === '0') {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselNameTextbox').val();

                    if (!payload.ShipAgencyVesselName || !payload.CaptainName || !payload.ShipAgencyName || !payload.ShipAgencyAddress || !payload.ShipAgencyMobile) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }

                    updateMarineIncidentDetails(payload, function (data) {


                        if (data.statusCode === 204) {
                            showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                            homeView.router.navigate("/home-bgv/");
                        }
                    })
                } else {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselName').val();

                    if (!payload.ShipAgencyVesselName || !payload.CaptainName || !payload.ShipAgencyName || !payload.ShipAgencyAddress || !payload.ShipAgencyMobile) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }

                    getMyVessels(3, function (vesselsList) {

                        let vesselList = (vesselsList);
                        vesselList.Result.forEach(function (vessel) {
                            if (vessel.Name === $$('#ShipAgencyVesselName').val()) {
                                payload.VesselID = vessel.ID;
                                updateMarineIncidentDetails(payload, function (data) {


                                    if (data.statusCode === 204) {
                                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                                        homeView.router.navigate("/home-bgv/");
                                    }
                                })
                            }
                        })

                    })
                }
            } else {
                if (payload.IsQatariRegistration === '0') {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselNameTextbox').val();

                    if (!payload.ShipAgencyVesselName || !payload.CaptainName || !payload.ShipAgencyName || !payload.ShipAgencyAddress || !payload.ShipAgencyMobile) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }

                    postMarineIncidentRequest(payload, function (data) {


                        if (data.statusCode === 201) {
                            showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                            homeView.router.navigate("/home-bgv/");
                        }
                    })
                } else {
                    payload.ShipAgencyVesselName = $$('#ShipAgencyVesselName').val();

                    if (!payload.ShipAgencyVesselName || !payload.CaptainName || !payload.ShipAgencyName || !payload.ShipAgencyAddress || !payload.ShipAgencyMobile) {
                        app.dialog.alert('Please fill all the mandatory fields', 'Alert');
                        return;
                    }

                    getMyVessels(3, function (vesselsList) {

                        let vesselList = (vesselsList);
                        vesselList.Result.forEach(function (vessel) {
                            if (vessel.Name === $$('#ShipAgencyVesselName').val()) {
                                payload.VesselID = vessel.ID;
                                postMarineIncidentRequest(payload, function (data) {


                                    if (data.statusCode === 201) {
                                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                                        homeView.router.navigate("/home-bgv/");
                                    }
                                })
                            }
                        })

                    })
                }
            }
        }

    })

    $$(document).on('click', '#cancel-marine', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateMarineIncidentDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })
        }

    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

    function setQatariRegistration(id) {
        console.log(id);
    }

});


// Mortrage Request
$$(document).on('page:init', '.page[data-name="mortrage-request"]', function (e) {

    setLanguage();


    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    isFile = false;
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);
    })

    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();

    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getMortgageDetails(request.id, function (data) {


                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }


                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                attachmentsGuid = data.Result[0].AttachmentID;
                mortgageAttachments(data.Result[0].AttachmentID);
                $$('#Mortgage').val(data.Result[0].Mortgage);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-mortgage').hide();
                    $$('#clear').hide();
                }
            })

        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-mortgage').hide();
            $$('#clear').hide();
        }
    }, 1000)

    $$(document).on('click', '#submit-mortgage', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.Mortgage = $$('#Mortgage').val();
        payload.AttachmentId = attachmentsGuid;

        console.log(payload);

        attachments.letter = $$('#mortgage-attachment-letter').val();
        attachments.letterBank = $$('#mortgage-attachment-letterBank').val();
        attachments.contract = $$('#mortgage-attachment-contract').val();
        attachments.original = $$('#mortgage-attachment-original').val();
        attachments.payment = $$('#mortgage-attachment-payment').val();

        if (!payload.Mortgage) {
            app.dialog.alert('Please fill all the mandatory fields', 'Alert');
            return;
        }

        if (!attachments.letter || !attachments.letterBank || !attachments.contract || !attachments.original) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateMortgageDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {
                postMortrageRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }
        }

    })

    $$(document).on('click', '#cancel-mort', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateMortgageDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })
        }
    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })
});

// Non Encumbrance
$$(document).on('page:init', '.page[data-name="non-encumbrance"]', function (e) {
    setLanguage();

    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    isFile = false;
    getVesselDetails(vesselId, function (data) {
        setupVesselInfo(data, e);
        setOwnerDetails(e);
    })

    $$('#request-status-container').hide();
    $$('#cancel').hide();
    $$('#comments-section').hide();
    $$('#payment-btn').hide();
    $$('.attachment-delete-icon').hide();

    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getNonEncumbranceDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }

                if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                    $$('#payment-btn').show();
                }

                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                }

                console.log(data.Result[0]);
                attachmentsGuid = data.Result[0].AttachmentID;
                nonEncumbranceAttachments(data.Result[0].AttachmentID);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    $$('input').prop('disabled', true);
                    $$('textarea').prop('disabled', true);
                    $$('#submit-encumbrance').hide();
                    $$('#clear').hide();
                }
            })

        }
        if (localStorage.getItem('isEdit') === 'false') {
            $$('input').prop('disabled', true);
            $$('textarea').prop('disabled', true);
            //$$('#ShipAgencyVesselNameTextbox').prop('disabled', true);
            $$('#submit-encumbrance').hide();
            $$('#clear').hide();
        }
    }, 1000)

    $$(document).on('click', '#submit-encumbrance', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');;
        payload.AttachmentId = attachmentsGuid;
        console.log(payload);

        attachments.letter = $$('#encumbrance-attachment-letter').val();
        attachments.letterBank = $$('#encumbrance-attachment-letterBank').val();
        attachments.original = $$('#encumbrance-attachment-original').val();
        attachments.payment = $$('#encumbrance-attachment-payment').val();

        if (!attachments.letter || !attachments.letterBank || !attachments.original) {
            app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
            return;
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateNonEncumbranceDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            } else {
                postNonEncumbranceDetails(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    }
                })
            }
        }
    })

    $$(document).on('click', '#cancel-non', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateNonEncumbranceDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })
        }
    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })
});

// Safe Manning Certificate
$$(document).on('page:init', '.page[data-name="safeManning-certificate"]', function (e) {

    setLanguage();



    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    isFile = false;
    getPorts(function (portsList) {
        getInsurers(function (insurersList) {
            getVesselDetails(vesselId, function (data) {

                // let ports = JSON.parse(portsList);
                let ports = (portsList);
                // let insurers = JSON.parse(insurersList);
                let insurers = (insurersList);
                data.Result[0].ports = ports.Result;
                data.Result[0].insurers = insurers.Result;

                // var data = JSON.stringify(data);
                setupVesselInfo(data, e);
            })
        })
        setOwnerDetails(e);
    })

    $$(document).on('click', '#submit', function (e) {
        var payload = {};
        payload.OwnerID = '3';
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.CrewAccommodation = $$('#CrewAccommodation').val();
        payload.IsUMSCertificate = $$("input[name='UMSCertificate']:checked").val();
        payload.IsNavigationalWatch = $$("input[name='navigationalWatch']:checked").val();
        payload.IsBridgeControl = $$("input[name='bridgeControl']:checked").val();
        payload.ExternalCommunication = $$("input[name='ExternalCommunication']:checked").val();
        payload.port = '1';
        console.log(payload);

        postSafeManningRequest(payload, function (data) {

            showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
            homeView.router.navigate("/");
        })
    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })
});

// CSR Page
$$(document).on('page:init', '.page[data-name="csr"]', function (e) {

    setLanguage();

    let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
    isFile = false;
    getPorts(function (portsList) {
        getVesselDetails(vesselId, function (vesselDetails) {
            console.log(vesselDetails);
            // var data = JSON.parse(vesselDetails);
            var data = (vesselDetails);
            setTimeout(function () {
                // if (JSON.parse(vesselDetails).Result[0].IsDeleted) {
                if ((vesselDetails).Result[0].IsDeleted) {
                    // $$('#shipDeletionDateContainer').show();
                    $$('#ShipDeletionDate').prop('disabled', true);
                    // $$('#ShipDeletionDate').val(moment(JSON.parse(vesselDetails).Result[0].ModifiedDate).format('YYYY-MM-DD'));
                    $$('#ShipDeletionDate').val(moment((vesselDetails).Result[0].ModifiedDate).format('YYYY-MM-DD'));
                } else {
                    $$('#shipDeletionDateContainer').hide();
                }
            }, 1000)
            getAllClasses(function (classesList) {
                let classes = JSON.parse(classesList);
                let ports = JSON.parse(portsList);
                data.Result[0].ports = ports.Result;
                data.Result[0].classes = classes.Result;
                // var data = JSON.stringify(data);
                setupVesselInfo(data, e);
                app.accordion.open($$(".page-current .accordion-item")[0]);

            })


        })
        $$('#request-status-container').hide();
        $$('#cancel').hide();
        $$('#comments-section').hide();
        $$('#payment-btn').hide();
        $$('.attachment-delete-icon').hide();
        setOwnerDetails(e);
    })

    setTimeout(function () {
        if (isEdit) {
            var request = JSON.parse(localStorage.getItem('request-details-data'));
            getCSRDetails(request.id, function (data) {

                localStorage.setItem('itemId', data.Result[0].ID);
                $$('#request-status-container').show();
                $$('#request-status')[0].innerHTML = data.Result[0].RequestStatus;
                $$('#ShipManagementCert').val(data.Result[0].MgtCertificateId);
                $$('#DocumentOfAppliance').val(data.Result[0].ApplianceDocId);
                $$('#ShipSecurityCert').val(data.Result[0].SecCertificateId);

                $$('#commentsFromVesselOwnerContainer').hide();
                $$('#flagInspectorContainer').hide();
                $$('#commentsToVesselOwnerContainer').hide();
                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    $$('#commentsToVesselOwnerContainer').show();
                    $$('#commentsToVesselOwner')[0].innerHTML = data.Result[0].CommentsToVesselOwner;

                } else {
                    $$('#ShipManagementCert').prop('disabled', true);
                    $$('#DocumentOfAppliance').prop('disabled', true);
                    $$('#ShipSecurityCert').prop('disabled', true);
                }
                if (data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems') {
                    $$('#comments-section').show();
                    $$('#commentsFromVesselOwnerContainer').show();
                    $$('#flagInspectorContainer').show();
                    $$('#commentsFromVesselOwner')[0].innerHTML = data.Result[0].CommentsFromVesselOwner;
                    $$('#flagInspectorComments')[0].innerHTML = data.Result[0].FlagInspectorComments;
                }


                if (localStorage.getItem('isEdit') === 'false') {
                    if (data.Result[0].RequestStatus === 'PendingOnNavigationSectionHeadApproval') {
                        $$('#cancel').show();
                    }
                } else {
                    if (data.Result[0].RequestStatus === 'PendingOnPayment') {
                        $$('#payment-btn').show();
                    }
                }



                attachmentsGuid = data.Result[0].AttachmentID;
                setCSRAttachments(data.Result[0].AttachmentID);

                //data.Result[0].IsBareboatCharter = false;
                //data.PortID = data.portId.toString();
                setTimeout(function () {
                    $$('#OwnerIdentification').val(data.Result[0].OwnerIdentification);
                    $$('#Charterer').val(data.Result[0].Charterer);
                    $$('#ChartererAddress').val(data.Result[0].ChartererAddress);
                    $$('#Remarks').val(data.Result[0].Remarks);
                    $$('#csrNumber').val(data.Result[0].CSRNumber);

                }, 2000)



                if (data.Result[0].IsBareboatCharter) {
                    $$("input[name='isBareboatCharter'][value='true']").prop('checked', true);
                } else {
                    $$("input[name='isBareboatCharter'][value='false']").prop('checked', true);
                    $$('#chartererAddress').hide();
                    $$('#chartererName').hide();
                }
                if (data.Result[0].IsBareBoatRenter) {
                    $$("input[name='isBareBoatRenter'][value='true']").prop('checked', true);
                } else {
                    $$("input[name='isBareBoatRenter'][value='false']").prop('checked', true);
                    $$('#renterName').hide();
                    $$('#renterAdd').hide();
                }


                $$("#PortID").val(data.Result[0].PortId.toString());
                $$("#RenterName").val(data.Result[0].RenterName);
                $$("#RenterAddress").val(data.Result[0].RenterAddress);
                $$("#RenterIMO").val(data.Result[0].RenterIMO);

                if (data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                    data.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                    data.Result[0].RequestStatus === 'RequireUserUpdatesBySeafarerSectionHead') {
                    $$('.attachment-delete-icon').show();
                } else {
                    setTimeout(function () {
                        $$('input').prop('disabled', true);
                        $$('textarea').prop('disabled', true);
                        $$('#submit-csr').hide();
                        $$('#clear').hide();
                        $$('#PortID').prop('disabled', true);
                        $$('#ShipManagementCert').prop('disabled', true);
                        $$('#DocumentOfAppliance').prop('disabled', true);
                        $$('#ShipSecurityCert').prop('disabled', true);
                    }, 2000)
                }
            })
        }
        if (localStorage.getItem('isEdit') === 'false') {
            setTimeout(function () {
                $$('input').prop('disabled', true);
                $$('textarea').prop('disabled', true);
                $$('#PortID').prop('disabled', true);
                $$('#ShipManagementCert').prop('disabled', true);
                $$('#DocumentOfAppliance').prop('disabled', true);
                $$('#ShipSecurityCert').prop('disabled', true);
                $$('#submit-csr').hide();
                $$('#clear').hide();
            }, 2000)

        }

    }, 1000)

    $$(document).on('click', '#submit-csr', function (e) {
        let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
        var payload = {};
        var attachments = {};
        payload.OwnerID = owner.ID;
        payload.VesselID = localStorage.getItem('vessel-selected-for-creating-new-request');
        payload.OwnerIdentification = $$('#OwnerIdentification').val();
        payload.Charterer = $$('#Charterer').val();
        payload.ChartererAddress = $$('#ChartererAddress').val();
        payload.Remarks = $$('#Remarks').val();
        payload.isBareboatCharter = $$("input[name='isBareboatCharter']:checked").val();
        payload.isBareBoatRenter = $$("input[name='isBareBoatRenter']:checked").val();
        payload.PortID = $$("#PortID").val();
        payload.RenterName = $$("#RenterName").val();
        payload.RenterAddress = $$("#RenterAddress").val();
        payload.RenterIMO = $$("#RenterIMO").val();
        payload.AttachmentId = attachmentsGuid;
        payload.isBareboatCharter = payload.isBareboatCharter == 'true' ? '1' : '0';
        payload.isBareBoatRenter = payload.isBareBoatRenter == 'true' ? '1' : '0';
        payload.OwnerIdentification = $$("#OwnerIdentification").val();
        payload.ApplianceDoc = $$("#DocumentOfAppliance").val();
        payload.MgtCertificate = $$("#ShipManagementCert").val();
        payload.SecCertificate = $$("#ShipSecurityCert").val();
        payload.CSRNumber = $$("#csrNumber").val();
        //payload.ShipDeletionDate = moment($$('#ShipDeletionDate').val()).format('MM/DD/YYYY');
        console.log(payload);

        attachments.letter = $$('#csr-attachment-letter').val();
        attachments.registry = $$('#csr-attachment-registry').val();
        attachments.appliance = $$('#csr-attachment-appliance').val();
        attachments.safety = $$('#csr-attachment-safety').val();
        attachments.security = $$('#csr-attachment-security').val();

        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;

            if (!attachments.letter || !attachments.registry || !attachments.appliance || !attachments.safety || !attachments.security) {
                app.dialog.alert('Please upload all the mandatory attachments', 'Alert');
                return;
            }

            if (isEdit) {
                payload.itemID = localStorage.getItem('itemId');
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionHead') {
                    payload.RequestStatus = 'PendingOnNavigationSectionHeadApproval';
                }
                if ($$('#request-status')[0].innerHTML === 'RequireUserUpdatesByNavigationSectionEmployee') {
                    payload.RequestStatus = 'PendingOnNavigationSectionEmployeeApproval';
                }
                updateCSRDetails(payload, function (data) {


                    if (data.statusCode === 204) {
                        showToast('Request ID ' + localStorage.getItem('itemId') + ' has updated successfully');
                        homeView.router.navigate("/home-bgv/");
                    } else if (data.statusCode === 500) {
                        showToast('Internal Server Error');
                    }
                })
            } else {
                postCSRRequest(payload, function (data) {


                    if (data.statusCode === 201) {
                        showToast('Request ID ' + data.Result[0].ID + ' has submitted successfully');
                        homeView.router.navigate("/home-bgv/");
                    } else if (data.statusCode === 500) {
                        showToast('Internal Server Error');
                    }
                })
            }
        }
    })

    $$(document).on('click', '#cancel-csr', function (e) {
        let payload = {
            itemID: localStorage.getItem('itemId'),
            VesselID: vesselId.trim(),
            RequestStatus: 'Canceled'
        }
        if (e.handled !== true) { // This will prevent event triggering more then once
            e.handled = true;
            updateCSRDetails(payload, function (data) {


                if (data.statusCode === 204) {
                    showToast('Request ID ' + localStorage.getItem('itemId') + ' has cancelled successfully');
                    homeView.router.navigate("/home-bgv/");
                }
            })
        }

    })

    $$(document).on('click', '#clear', function (e) {
        clear();
    })

});

// My Vessels
$$(document).on('page:init', '.page[data-name="my-vessels"]', function (e) {

    setLanguage();

    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    getMyVessels(owner.ID, function (data) {

        if (data.Result.length === 0) {
            app.dialog.alert('Sorry, There are no vessels yet.', 'Alert');
        }
        setMyVesselsList(data, e);

    })
});

// Vessels Certificate
$$(document).on('page:init', '.page[data-name="vessels-certificate"]', function (e) {

    setLanguage();

    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    getMyVessels(owner.ID, function (data) {

        if (data.Result.length === 0) {
            app.dialog.alert('Sorry, There are no vessels yet.', 'Alert');
        }
        setVesselsCertificateList(data, e);

    })
});

// My Requests
var requestsAll = [];
$$(document).on('page:reinit', '.page[data-name="my-requests"]', function (e) {
    $$("#requestSearch input").removeAttr("disabled");


});

$$(document).on('page:init', '.page[data-name="my-requests"]', function (e) {

    setLanguage();

    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    localStorage.setItem('hideloader', false);
    app.preloader.show();
    let serviceCount = 0;


    var searchbar = app.searchbar.create({
        // el: '#requestSearch',
        // searchContainer: '#vesselServicesList',
        // searchIn: '.item-title',
        // on: {
        //     search(sb, query, previousQuery) {
        //         console.log(query, previousQuery);
        //     }
        // }
    });



    var requestArray = [];
    requestPageEvent = e;
    setTimeout(() => {
        getCertificateList(function (vesselServicesData) {

            localStorage.setItem('vesselServices', vesselServicesData);
            var vesselServicesData = JSON.parse(vesselServicesData);
            vesselServices = vesselServicesData;
            console.log(vesselServices);

            vesselServicesData.Result.forEach(function (element, i) {

                let requestData = {
                    "OwnerID": owner.ID,
                    "ListGUID": element.ListGuid,
                    //"Name": $$(e).children()[1].innerHTML
                }

                getAllRequestsByOwnerID(requestData, function (data) {

                    var obj = {
                        Service: element.Name,
                        Requests: []
                    }

                    if (element.Name === 'Certificate of Specification Amend') {
                        obj.icon = "icon-application-for-certificate-of-qatar-registry- licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getSpecificationAmendDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Transfer Ownership') {
                        obj.icon = "icon-application-of-ownership-transfer licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getOwnershipTransferDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })


                    } else if (element.Name === 'Ship Deletion') {
                        obj.icon = "icon-issuing-vessel-deletion-certificate licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getShipDeletionDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Certificate of Non Encumbrance') {
                        obj.icon = "icon-issuing-a-certificate-of-non-encumbrance licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getNonEncumbranceCertDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Continuous Synopsis Record') {
                        obj.icon = "icon-issuing-continuous-synopsis-record licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getCSRDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Mortgage Registration') {
                        obj.icon = "icon-application-of-mortgage-registration licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getMortgageDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Non Encumbrance') {
                        obj.icon = "icon-application-of-non-encumbrance licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getNonEncumbranceDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Safe Manning') {
                        obj.icon = "icon-safe-manning-certificate licon"
                    } else if (element.Name === 'Derating Certificate') {
                        obj.icon = "icon-derating-certificate licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getDerratingDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Exemption Certificate') {
                        obj.icon = "icon-issuing-exemption-certificates licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getExemptionDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Safety Insurance For Oil Pollution Damage') {
                        obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getSafetyInsuranceDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Extension Certificate') {
                        obj.icon = "icon-issuing-a-certificate-of-authorization licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getExtensionDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Lost Replacement') {
                        obj.icon = "icon-application-for-issue-the-certificate-of licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getLostAndReplacementDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'Marine Incident') {
                        obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getMarineIncidentDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'To Whom It May Concern Certificate') {
                        obj.icon = "icon-issuing-a-certificate-to-whom-it licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getToWhomDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    } else if (element.Name === 'NOL Request') {
                        obj.icon = "icon-issuing-nol-for-the-announcement-of- licon"

                        if (data.Result.length === 0) {
                            serviceCount += 1;
                        }

                        data.Result.forEach(function (request, i) {
                            getNOLDetails(request.ID, function (detail) {
                                // let detail = JSON.parse(details);
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            })
                            if (i + 1 === data.Result.length) {
                                serviceCount += 1;
                            }
                        })
                    }

                    if (element.Name !== 'Safe Manning' && element.Name !== 'Provisional Registery' && element.Name !== 'Permenant Registery' && element.Name !== 'Trading Certificate' && element.Name !== 'Annual Fee') {
                        requestArray.push(obj);
                    }


                    console.log(vesselServicesData.Result.length);
                    if (vesselServicesData.Result.length === i + 1) {
                        allRequests = requestArray
                        let interval = true;
                        setTimeout(function () {
                            if (serviceCount === 15 && interval) {
                                interval = false;
                                showRequests(requestArray, e, 'requests');
                            }
                        }, 8000)
                    }

                })
            }, this);



        })
    }, 200);

    setTimeout(function () {
        localStorage.setItem('hideloader', true);
        app.preloader.hide();
    }, 9000);

});

// Tasks List

$$(document).on('page:reinit', '.page[data-name="tasks-list"]', function (e) {
    $$("#taskSearch input").removeAttr("disabled");


});

$$(document).on('page:init', '.page[data-name="tasks-list"]', function (e) {

    setLanguage();

    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    localStorage.setItem('hideloader', false);
    app.preloader.show();
    let serviceCount = 0;
    taskPageEvent = e;


    var searchbar = app.searchbar.create({
        // el: '#taskSearch',
        // searchContainer: '#tasksList',
        // searchIn: '.item-title',
        // on: {
        //     search(sb, query, previousQuery) {
        //         console.log(query, previousQuery);
        //     }
        // }
    });

    var requestArray = [];
    requestPageEvent = e;
    getCertificateList(function (vesselServicesData) {

        localStorage.setItem('vesselServices', vesselServicesData);
        var vesselServicesData = JSON.parse(vesselServicesData);
        vesselServices = vesselServicesData;
        console.log(vesselServices);

        vesselServicesData.Result.forEach(function (element, i) {

            let requestData = {
                "OwnerID": owner.ID,
                "ListGUID": element.ListGuid,
                //"Name": $$(e).children()[1].innerHTML
            }

            getAllRequestsByOwnerID(requestData, function (data) {
                var obj = {
                    Service: element.Name,
                    Requests: []
                }

                if (element.Name === 'Lost Replacement') {
                    obj.icon = "icon-application-for-issue-the-certificate-of licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getLostAndReplacementDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }

                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Marine Incident') {
                    obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getMarineIncidentDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'To Whom It May Concern Certificate') {
                    obj.icon = "icon-issuing-a-certificate-to-whom-it licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getToWhomDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'NOL Request') {
                    obj.icon = "icon-issuing-nol-for-the-announcement-of- licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNOLDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Certificate of Specification Amend') {
                    obj.icon = "icon-application-for-certificate-of-qatar-registry- licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getSpecificationAmendDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Transfer Ownership') {
                    obj.icon = "icon-application-of-ownership-transfer licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getOwnershipTransferDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })


                } else if (element.Name === 'Ship Deletion') {
                    obj.icon = "icon-issuing-vessel-deletion-certificate licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getShipDeletionDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Certificate of Non Encumbrance') {
                    obj.icon = "icon-issuing-a-certificate-of-non-encumbrance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNonEncumbranceCertDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Continuous Synopsis Record') {
                    obj.icon = "icon-issuing-continuous-synopsis-record licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getCSRDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Mortgage Registration') {
                    obj.icon = "icon-application-of-mortgage-registration licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getMortgageDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Non Encumbrance') {
                    obj.icon = "icon-application-of-non-encumbrance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNonEncumbranceDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Safe Manning') {
                    obj.icon = "icon-safe-manning-certificate licon"
                } else if (element.Name === 'Derating Certificate') {
                    obj.icon = "icon-derating-certificate licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getDerratingDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Exemption Certificate') {
                    obj.icon = "icon-issuing-exemption-certificates licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getExemptionDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Safety Insurance For Oil Pollution Damage') {
                    obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getSafetyInsuranceDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Extension Certificate') {
                    obj.icon = "icon-issuing-a-certificate-of-authorization licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getExtensionDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                }
                if (element.Name !== 'Safe Manning' && element.Name !== 'Provisional Registery' && element.Name !== 'Permenant Registery' && element.Name !== 'Trading Certificate' && element.Name !== 'Annual Fee') {
                    requestArray.push(obj);
                }


                console.log(vesselServicesData.Result.length);
                if (vesselServicesData.Result.length === i + 1) {
                    allRequests = requestArray
                    tasksAll = requestArray;
                    let interval = true;
                    setTimeout(function () {
                        if (serviceCount === 15 && interval) {
                            interval = false;
                            var Result = {
                                Result: requestArray
                            };

                            var vesselServices = tasksList_TPL(Result);

                            var t = e.detail.$el[0];

                            $$(t).find(".tasks-list").html(vesselServices);
                        }
                    }, 8000)
                }

            })
        }, this);
    })
    setTimeout(function () {
        localStorage.setItem('hideloader', true);
        app.preloader.hide();
    }, 9000)

});

// Home-bgv
$$(document).on('page:init', '.page[data-name="home-bgv"]', function (e) {

    setLanguage();
    var data = {
        Result: [{
            'ID': ''
        }]
    }

    localStorage.setItem('owner-data', JSON.stringify(data));


    ownerDetails_template = Template7.partials.ownerDetails.template;
    ownerDetails_TPL = Template7.compile(ownerDetails_template);

    myDetails_template = Template7.partials.myDetails.template;
    myDetails_TPL = Template7.compile(myDetails_template);
})

// Certificates
$$(document).on('page:init', '.page[data-name="certificates"]', function (e) {

    setLanguage();


})


function changeCauseOfDeletion(cause) {
    console.log(cause);

    if (cause === 'Sold for Foreign Owner') {
        $$('#evidance-sink-attachment-container').hide();
        $$('#bill-of-sale-attachment-container').show();
        $$('#letter-new-flag').show();
    } else if (cause === 'Scrap') {
        $$('#evidance-sink-attachment-container').show();
        $$('#letter-new-flag').hide();
        $$('#bill-of-sale-attachment-container').hide();

    } else {
        $$('#evidance-sink-attachment-container').show();
        $$('#bill-of-sale-attachment-container').hide();
        $$('#letter-new-flag').hide();
    }

}

function setBareboatCharter(value) {
    console.log(value);
    if (localStorage.getItem('isEdit') !== 'false') {
        if (value === 'true') {
            $$('#chartererAddress').show();
            $$('#chartererName').show();
        } else {
            $$('#chartererAddress').hide();
            $$('#chartererName').hide();
        }
    }
}

function setBareboatRenter(value) {
    if (localStorage.getItem('isEdit') !== 'false') {
        if (value === 'true') {
            $$('#renterName').show();
            $$('#renterAdd').show();
        } else {
            $$('#renterName').hide();
            $$('#renterAdd').hide();
        }
    }
}

// Derating Certificate
function setQatariRegistration(id) {
    console.log(id);
    isQatariRegistration = id;
    console.log(testData);
    if (id === '0') {
        $$('#showVesselsDropdown').hide();
        $$('#vesselNameTextbox').show();
        $$('#OfficialNumberTextbox').show();
        $$('#OfficialNumber').hide();
        // $$('#VesselOfficialNumber')[0].innerHTML = JSON.parse(data).Result[0].OfficialNumber;
    } else {
        $$('#showVesselsDropdown').show();
        $$('#vesselNameTextbox').hide();
        $$('#OfficialNumberTextbox').hide();
        $$('#OfficialNumber').show();
    }
}

function goToRequestDetails(ID, serviceName, vesselID, isEdit) {
    localStorage.setItem('isEdit', isEdit);
    $$('#engineSpecsInner').show();
    $$('#engineSpecsTextboxInner').show();


    let id = ID;
    let vesselId = vesselID;





    localStorage.setItem('vessel-selected-for-creating-new-request', vesselId);

    getVesselDetails(vesselId, function (data) {
        // let vesselType = JSON.parse(data).Result[0].TypeofVessel;
        let vesselType = (data).Result[0].TypeofVessel;
        console.log(vesselType);
        setTimeout(function () {
            if (vesselType.trim() === 'barge') {
                $$('#engineSpecsInner').hide();
                $$('#engineSpecsTextboxInner').hide();

            }
        }, 100)
    })



    let requestData = JSON.parse(localStorage.getItem("request-data"));

    let requestDetailData = {
        id: id,
        name: serviceName
    }

    localStorage.setItem('request-details-data', JSON.stringify(requestDetailData));

    //isEdit = true;
    checkEditMode(true);

    if (serviceName === 'Certificate of Specification Amend') {
        homeView.router.navigate('/certificate-of-specification-amend-request/');
    } else if (serviceName === 'Transfer Ownership') {
        homeView.router.navigate('/ownership-transfer-request/');
    } else if (serviceName === 'Ship Deletion') {
        homeView.router.navigate('/issue-vessel-deletion-certificate-request/');
    } else if (serviceName === 'Certificate of Non Encumbrance') {
        homeView.router.navigate('/issue-non-encumbrance-certificate/');
    } else if (serviceName === 'Continuous Synopsis Record') {
        homeView.router.navigate('/csr-certificate/');
    } else if (serviceName === 'Mortgage Registration') {
        homeView.router.navigate('/mortgage-request/');
    } else if (serviceName === 'Non Encumbrance') {
        homeView.router.navigate('/non-encumbrance-certificate/');
    } else if (serviceName === 'Safe Manning') {
        homeView.router.navigate('/safe-manning-certificate/');
    } else if (serviceName === 'Derating Certificate') {
        homeView.router.navigate('/derrating-certificate/');
    } else if (serviceName === 'Exemption Certificate') {
        homeView.router.navigate('/issue-exemption-certificates/');
    } else if (serviceName === 'Safety Insurance For Oil Pollution Damage') {
        homeView.router.navigate('/safety-insurance-certificate/');
    } else if (serviceName === 'Extension Certificate') {
        homeView.router.navigate('/issue-authorization-extension-relaxation-certificate/');
    } else if (serviceName === 'Lost Replacement') {
        homeView.router.navigate('/lost-replacement-certificate/');
    } else if (serviceName === 'Marine Incident') {
        homeView.router.navigate('/marine-incident-certificate/');
    } else if (serviceName === 'To Whom It May Concern Certificate') {
        homeView.router.navigate('/issue-towhomconcern-certificate/');
    } else if (serviceName === 'NOL Request') {
        homeView.router.navigate('/issue-NOL-certificate/');
    }


}

function selectOwnerType(type) {
    console.log(type);
}

function selectVessel(selectedVessel) {

    getMyVessels(3, function (vesselsList) {

        let vesselList = (vesselsList);
        vesselList.Result.forEach(function (vessel) {
            if (vessel.Name === selectedVessel) {
                console.log(vessel.ID);
                //$$('#VesselOfficialNumber')[0].innerHTML = vessel.OfficialNumber;
                $$('#VesselOfficialNumberTextbox').val(vessel.OfficialNumber);
                $$('#VesselOfficialNumber')[0].innerHTML = vessel.OfficialNumber;
                // $$('#VesselimoNumber')[0].innerHTML = vessel.IMONumber;
            }
        })
    })
}

function showRequests(requestArray, e, from) {
    let array = [];

    console.log(requestArray);
    requestArray.forEach(function (v, i) {
        if (v.Requests.length != 0) {
            array.push(v);
        }
    })
    var Result = {
        Result: array
    };

    if (from == 'requests') {
        requestsAll = array;

    }

    var vesselServices = myRequests_TPL(Result);

    var t = e.detail.$el[0];

    $$(t).find(".vessel-services-list").html(vesselServices);


}

function checkFile(fileName, fileInput, fileType) {
    console.log(fileInput);
    var file = $$('#' + fileInput)[0].files[0];
    console.log($$('#' + fileInput)[0].files[0]);
    $$('#' + fileName).val(file.name);
    uploadFileToServer(file, isFile, fileType);
    isFile = true;
}

function getAllRequest(e) {
    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    console.log(e);
    let id = $$(e).children()[2].innerHTML;

    let requestData = {
        "OwnerID": owner.ID,
        "ListGUID": id,
        "Name": $$(e).children()[1].innerHTML
    }
    localStorage.setItem('request-data', JSON.stringify(requestData));

    homeView.router.navigate('/owner-requests/');


}



function selectNameOfOperation(value) {
    var found = false;
    console.log(value);
    natureOfOperationArr.forEach(function (v, i) {
        if (v == value) {
            found = true;
            natureOfOperationArr.splice(i, 1)
        }
    })
    if (found) {

    } else {
        natureOfOperationArr.push(Number(value));
    }
    console.log(natureOfOperationArr);

}

var fileGuid = function (isFile) {
    var guid;

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    guid = s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();

    return guid;
}

function uploadFileToServer(file, isFile, fileType) {
    if (!isFile && !isEdit) {
        attachmentsGuid = fileGuid(isFile);
    }
    console.log(attachmentsGuid);


    if (file) {

        var fd = new FormData();
        // append all the files u need here
        fd.append('testfile', file);

        //insert here the GUID of ur files
        fd.append('fileGUID', attachmentsGuid);


        uploadFile(fd, function (data) {

            let FileType = "Test my API file type";

            var payload = {
                "FileName": file.name,
                "FileGUID": attachmentsGuid,
                "FileType": fileType
            };

            uploadDocumentAddFile(payload, function (data) {

                showToast(data.slice(0, data.indexOf('.')));

            });

        })
    } else {
        console.log('No File Choosen');
        app.dialog.alert('Please choose file to upload', 'Alert');
    }

}

function setMyVesselsList(data, e) {
    console.log(data);
    console.log(e);

    var html = myVessels_TPL(data);

    var t = e.detail.$el[0];


    $$(t).find(".myVessels").html(html);

    // open vessel accordion
    app.accordion.open($$(t).find(".myVessels"));

}

function setVesselsCertificateList(data, e) {

    let serviceName = localStorage.getItem('service-selected-for-reating-new-request');
    let vesselsArray = [];

    console.log(data);
    console.log(serviceName);

    if (serviceName === 'Derating Certificate' || serviceName === 'Marine Incident' || serviceName === 'NOL Request') {
        vesselsArray = data.Result;
    } else {
        data.Result.forEach(function (vessel) {

            if ((vessel.IsPermanent || vessel.IsProvisional) && !vessel.IsDeleted) {
                if (serviceName === 'Certificate of Specification Amend' || serviceName === 'Exemption Certificate' || serviceName === 'Safety Insurance For Oil Pollution Damage' ||
                    serviceName === 'Transfer Ownership' || serviceName === 'Extension Certificate' || serviceName === 'To Whom It May Concern Certificate' ||
                    serviceName === 'Ship Deletion' || serviceName === 'Lost Replacement') {

                    vesselsArray.push(vessel);
                }

                if (vessel.GT > 500 && serviceName === 'Continuous Synopsis Record') {
                    vesselsArray.push(vessel);
                }

                if (vessel.IsMortgaged && serviceName === 'Non Encumbrance') {
                    vesselsArray.push(vessel);
                }
                if (!vessel.IsMortgaged) {
                    if (serviceName === 'Mortgage Registration' || serviceName === 'Certificate of Non Encumbrance')
                        vesselsArray.push(vessel);
                }
            } else if ((vessel.IsPermanent || vessel.IsProvisional) && vessel.IsDeleted) {
                if (vessel.GT > 500 && serviceName === 'Continuous Synopsis Record') {
                    vesselsArray.push(vessel);
                }
            }
            console.log(vesselsArray);
        })
    }

    var html = vesselsCertificate_TPL({
        vessels: vesselsArray
    });
    var t = e.detail.$el[0];
    $$(t).find(".vessel-certificate-list").html(html);


}

function goToCertificateDetails(e) {
    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    console.log($$(e).children()[2].innerHTML);
    let vesselId = $$(e).children()[2].innerHTML;
    let vesselType = $$(e).children()[3].innerHTML;
    console.log(vesselType);

    // setTimeout(function () {
    //   if (vesselType.trim() === 'barge') {
    //     $$('#engineSpecsInner').hide();
    //     $$('#engineSpecsTextboxInner').hide();

    //   }
    // }, 1000)

    localStorage.setItem('vessel-selected-for-creating-new-request', vesselId.trim());
    localStorage.setItem('isEdit', null);

    console.log(localStorage.getItem('service-selected-for-reating-new-request'));
    let serviceName = localStorage.getItem('service-selected-for-reating-new-request');

    let payload = {

    }

    getCertificateList(function (vesselServicesData) {
        console.log(vesselServicesData)
        let data = JSON.parse(vesselServicesData);
        //let data = (vesselServicesData);

        data.Result.forEach(function (service) {
            if (serviceName === service.Name) {
                console.log(service);
                let payload = {
                    ListGUID: service.ListGuid,
                    OwnerID: owner.ID,
                    VesselID: vesselId.trim()
                }

                requestIsExist(payload, function (data) {


                    let isExist = data.isExist;

                    if (serviceName === 'Certificate of Specification Amend') {
                        if (isExist === 0) {
                            homeView.router.navigate('/certificate-of-specification-amend-request/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }
                    } else if (serviceName === 'Transfer Ownership') {
                        if (vesselType === 'Mortgaged' || vesselType === 'mortgaged' || vesselType === 'mortgage') {
                            app.dialog.alert('Ownership can not be transferred, Vessel is mortgaged.', 'Alert');
                        } else {
                            if (isExist === 0) {
                                homeView.router.navigate('/ownership-transfer-request/');
                            } else {
                                app.dialog.alert('You cannot apply for this service twice', 'Alert');
                            }
                        }
                    } else if (serviceName === 'Ship Deletion') {
                        if (isExist === 0) {
                            homeView.router.navigate('/issue-vessel-deletion-certificate-request/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }
                    } else if (serviceName === 'Certificate of Non Encumbrance') {
                        if (isExist === 0) {
                            homeView.router.navigate('/issue-non-encumbrance-certificate/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }

                    } else if (serviceName === 'Continuous Synopsis Record') {
                        homeView.router.navigate('/csr-certificate/');
                    } else if (serviceName === 'Mortgage Registration') {
                        if (isExist === 0) {
                            homeView.router.navigate('/mortgage-request/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }

                    } else if (serviceName === 'Non Encumbrance') {
                        if (isExist === 0) {
                            homeView.router.navigate('/non-encumbrance-certificate/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }
                    } else if (serviceName === 'Safe Manning') {
                        homeView.router.navigate('/safe-manning-certificate/');
                    } else if (serviceName === 'Derating Certificate') {
                        if (isExist === 0) {
                            homeView.router.navigate('/derrating-certificate/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }
                    } else if (serviceName === 'Exemption Certificate') {
                        homeView.router.navigate('/issue-exemption-certificates/');
                    } else if (serviceName === 'Safety Insurance For Oil Pollution Damage') {
                        if (vesselType === 'TANKER' || vesselType === 'LPG' || vesselType === 'Liquified Gas T') {
                            if (isExist === 0) {
                                homeView.router.navigate('/safety-insurance-certificate/');
                            } else {
                                app.dialog.alert('You cannot apply for this service twice', 'Alert');
                            }
                        } else {
                            app.dialog.alert('You cannot create safety insurance request with this vessel', 'Alert');
                        }
                    } else if (serviceName === 'Extension Certificate') {
                        homeView.router.navigate('/issue-authorization-extension-relaxation-certificate/');
                    } else if (serviceName === 'Lost Replacement') {
                        homeView.router.navigate('/lost-replacement-certificate/');
                    } else if (serviceName === 'Marine Incident') {
                        if (isExist === 0) {
                            homeView.router.navigate('/marine-incident-certificate/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }
                    } else if (serviceName === 'To Whom It May Concern Certificate') {
                        homeView.router.navigate('/issue-towhomconcern-certificate/');
                    } else if (serviceName === 'NOL Request') {
                        if (isExist === 0) {
                            homeView.router.navigate('/issue-NOL-certificate/');
                        } else {
                            app.dialog.alert('You cannot apply for this service twice', 'Alert');
                        }
                    }

                })
            }
        })

    })




}

function gotoVesselsList(service) {
    console.log(service);
    checkEditMode(false);
    localStorage.setItem('service-selected-for-reating-new-request', service);
    homeView.router.navigate('/vessels-certificate/');

}

function setupVesselInfo(data, e) {
    data.Result[0].CreatedDate = moment(data.Result[0].CreatedDate).format('DD-MM-YYYY');
    data = data.Result[0];
    data.shipDeletionPage = false;


    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    // let owner = localStorage.getItem('owner-data').Result[0]
    data.ownerName = owner.Name;
    data.ownerAddress = owner.Address;
    $$("div.page.page-current .vesselLink").html('<a href="/vessel-details/' + data.ID + '">' + data.Name + '</a>');

    if (homeView.router.url === '/issue-vessel-deletion-certificate-request/') {
        data.vesselName = true;
        data.vesselType = true;
        data.whereBuilt = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.officialNumber = false;
        data.registrationDate = false;
        data.registrationType = false;
        data.expiryDate = false;
        data.port = false;
        data.underConstruction = false;
        data.prevCallSign = false;
        data.keelLaid = false;
        data.previousRegistration = false;
        data.causeOfDeletion = true;
        data.commentsForDeletion = true;
        data.certificatePresentedTo = false;
        data.certificateReasons = false;
        data.isMortgage = false;
        data.mortgageSide = false;
    }
    if (homeView.router.url === '/certificate-of-specification-amend-request/') {
        data.vesselName = false;
        data.vesselType = false;
        data.whereBuilt = false;
        data.yearOfBuilt = false;
        data.imoNumber = false;
        data.officialNumber = false;
        data.registrationDate = false;
        data.registrationType = false;
        data.expiryDate = false;
        data.port = false;
        data.underConstruction = false;
        data.prevCallSign = false;
        data.keelLaid = false;
        data.previousRegistration = false;
        data.causeOfDeletion = false;
        data.commentsForDeletion = false;
        data.certificatePresentedTo = false;
        data.certificateReasons = false;
        data.isMortgage = false;
        data.mortgageSide = false;
        data.vesselNameTextbox = true;
    }
    if (homeView.router.url === '/issue-non-encumbrance-certificate/') {
        data.vesselName = true;
        data.vesselType = true;
        data.whereBuilt = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.prevCallSign = true;
        data.keelLaid = true;
        data.previousRegistration = true;

        data.depth = true;
        data.breadth = true;
        data.gt = true;
        data.lt = true;
        data.loa = true;

        data.engineDescription = true;
        data.numberofEngines = true;
        data.speedOfVessel = true;
        data.noofCylinders = true;
        data.noofShafts = true;
        data.engineMaker = true;
        data.engineYearMade = true;
    }

    if (homeView.router.url === '/issue-towhomconcern-certificate/') {
        data.vesselName = true;
        data.vesselType = false;
        data.whereBuilt = false;
        data.yearOfBuilt = false;
        data.imoNumber = false;
        data.officialNumber = true;
        data.registrationDate = false;
        data.registrationType = false;
        data.expiryDate = false;
        data.port = false;
        data.underConstruction = false;
        data.prevCallSign = true;
        data.keelLaid = false;
        data.previousRegistration = false;
        data.causeOfDeletion = false;
        data.commentsForDeletion = false;
        data.certificatePresentedTo = true;
        data.certificateReasons = true;
    }

    if (homeView.router.url === '/safety-insurance-certificate/') {
        data.vesselName = true;
        data.vesselType = false;
        data.whereBuilt = false;
        data.yearOfBuilt = false;
        data.imoNumber = false;
        data.officialNumber = true;
        data.registrationDate = false;
        data.registrationType = false;
        data.expiryDate = false;
        data.portLabel = true;
        data.underConstruction = false;
        data.prevCallSign = true;
        data.keelLaid = false;
        data.previousRegistration = false;
        data.causeOfDeletion = false;
        data.commentsForDeletion = false;
        data.certificatePresentedTo = false;
        data.certificateReasons = false;
        data.isMortgage = false;
        data.mortgageSide = false;
        data.securityType = true;
        data.securityDurationFrom = true;
        data.securityDurationTo = true;
        data.insurerName = true;
        data.insurerAddress = true;
        data.certificateExpiry = false;
    }

    if (homeView.router.url === '/issue-authorization-extension-relaxation-certificate/') {
        data.vesselName = true;
        data.vesselType = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.officialNumber = true;
        data.portLabel = true;
        data.grossTonage = true;
    }

    if (homeView.router.url === '/issue-NOL-certificate/') {
        data.vesselName = true;
        data.vesselType = true;
        data.vesselStatus = true;
        data.registrationType = false;
        data.expiryDate = true;
    }

    if (homeView.router.url === '/lost-replacement-certificate/') {
        data.vesselName = true;
        data.vesselType = true;
        data.vesselStatus = true;
        data.registrationType = false;
        data.expiryDate = true;
    }

    if (homeView.router.url === '/issue-exemption-certificates/') {
        data.vesselName = true;
        data.vesselType = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.officialNumber = true;
        //data.port = true;
        data.portLabel = true;
        data.grossTonage = true;
    }

    if (homeView.router.url === '/derrating-certificate/') {
        data.showVesselsDropdown = true;
        data.vesselNameTextbox = true;
        data.imoNumber = false;
        data.officialNumber = true;
        data.officialNumberTextbox = true;
        data.vesselRegistration = true;
    }

    if (homeView.router.url === '/marine-incident-certificate/') {
        data.vesselRegistration = true;
        data.captainName = true;
        data.shipAgencyName = true;
        data.shipAgencyAddress = true;
        data.shipAgencyEmail = true;
        data.shipAgencyMobile = true;
        data.showVesselsDropdown = true;
        data.vesselNameTextbox = true;
    }

    if (homeView.router.url === '/mortgage-request/') {
        data.vesselName = true;
        data.vesselType = true;
        data.whereBuilt = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.prevCallSign = true;
        data.keelLaid = true;
        data.previousRegistration = true;

        data.depth = true;
        data.breadth = true;
        data.gt = true;
        data.lt = true;
        data.loa = true;

        data.engineDescription = true;
        data.numberofEngines = true;
        data.speedOfVessel = true;
        data.noofCylinders = true;
        data.noofShafts = true;
        data.engineMaker = true;
        data.engineYearMade = true;
    }

    if (homeView.router.url === '/safe-manning-certificate/') {
        data.vesselName = true;
        data.vesselType = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.officialNumber = true;
        data.port = true;
        data.areaOfNavigation = true;
        data.specifyAreaOfNavigation = true;

        data.length = true;
        data.breadth = true;
        data.gt = true;
        data.callSign = true;
        data.certifiedCrewAccommodation = true;

        data.engineDescription = true;
        data.numberofEngines = true;
        data.speedOfVessel = true;
        data.navigationalWatch = true;
        data.holdUmsCertificate = true;
        data.bridgeControl = true;
        data.bhp = true;
    }

    if (homeView.router.url === '/csr-certificate/') {
        data.vesselName = true;
        data.imoNumber = true;
        data.officialNumber = true;
        data.registrationDate = true;
        data.port = true;
        data.csr = true;
        data.documentOfAppliance = true;
        data.shipManagementCert = true;
        data.shipSecurityCert = true;
        data.csrNumber = true;
    }

    if (homeView.router.url === '/ownership-transfer-request/') {
        //data.vesselName = true;
        data.vesselNameTextbox = true;
        data.vesselType = true;
        data.whereBuilt = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.expiryDate = true;
        data.underConstruction = true;
        data.prevCallSign = true;
        data.keelLaid = true;
        data.previousRegistration = true;
        data.vesselStatus = true;

        data.depth = true;
        data.breadth = true;
        data.gt = true;
        data.lt = true;
        data.loa = true;

        data.engineDescription = true;
        data.numberofEngines = true;
        data.speedOfVessel = true;
        data.noofCylinders = true;
        data.noofShafts = true;
        data.engineMaker = true;
        data.engineYearMade = true;
    }

    if (homeView.router.url === '/non-encumbrance-certificate/') {
        data.vesselName = true;
        data.vesselType = true;
        data.whereBuilt = true;
        data.yearOfBuilt = true;
        data.imoNumber = true;
        data.prevCallSign = true;
        data.keelLaid = true;
        data.previousRegistration = true;

        data.depth = true;
        data.breadth = true;
        data.gt = true;
        data.lt = true;
        data.loa = true;

        data.engineDescription = true;
        data.numberofEngines = true;
        data.speedOfVessel = true;
        data.noofCylinders = true;
        data.noofShafts = true;
        data.engineMaker = true;
        data.engineYearMade = true;
    }

    if (moment(data.ExpirationDate).format('DD-MM-YYYY') === 'Invalid date') {
        data.ExpirationDate = '-';
    } else {
        data.ExpirationDate = moment(data.ExpirationDate).format('DD-MM-YYYY');
    }
    data.KeelLaid = moment(data.KeelLaid).format('DD-MM-YYYY');

    getAllCountries(function (countries) {

        var countries_list = JSON.parse(countries);
        // var countries_list = (countries);
        data.countries = countries_list.Result;

        var html = vesselTPL(data);
        var vesselDimension = vesselDIM_TPL(data);
        var engineSpecs = engineSpecs_TPL(data);
        var htmlvesselDIM = vesselDIMTextbox_TPL(data);
        var htmlengineSpecsTextbox = engineSpecsTextbox_TPL(data);


        var t = e.detail.$el[0];

        $$(t).find(".vessel_info").html(html);
        $$(t).find(".vesselDimension").html(vesselDimension);
        $$(t).find(".engineSpecs").html(engineSpecs);
        $$(t).find(".vesselDIM-textbox").html(htmlvesselDIM);
        $$(t).find(".engineSpecs-textbox").html(htmlengineSpecsTextbox);

        // open vessel accordion
        //app.accordion.open($$(t).find(".vessel_info"));

        if (data.TypeofVessel === 'barge') {
            $$('#engineSpecsInner').hide();
            $$('#engineSpecsTextboxInner').hide();
        }

    })

}

function setOwnerDetails(e) {
    // let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    getOwnerDetails(1, function (data) {

        if (data.Result[0].OwnerType === 'Company') {
            $$('#transfer-attachment-input-commercialSeller').prop('disabled', true);
            $$('#qidOwner').hide();
            $$('#commercial-registration-attach-container').show();
            $$('#qid-seller-attachement').hide();
            $$('#commercial-reg-seller-attachement').show();
            $$('#transfer-qid-container').hide();
            $$('#transfer-cr-container').show();
            $$('#transfer-contact-container').show();
        } else {
            $$('#transfer-attachment-input-qidSeller').prop('disabled', true);
            $$('#crOwner').hide();
            $$('#qid-seller-attachement').show();
            $$('#commercial-reg-seller-attachement').hide();
            $$('#transfer-qid-container').show();
            $$('#transfer-cr-container').hide();
            $$('#transfer-contact-container').hide();
        }

        setTimeout(function () {
            $$('#vesselOwnerName')[0].innerHTML = data.Result[0].Name;
            $$('#vesselOwnerAddress')[0].innerHTML = data.Result[0].Address;
        }, 2000)

        var ownerDetails = ownerDetails_TPL(data.Result[0]);
        var myDetails = myDetails_TPL(data.Result[0]);

        var t = e.detail.$el[0];

        $$(t).find(".owner_details").html(ownerDetails);

        $$(t).find(".mydetails-details-block").html(myDetails);


    })
}

// Payment
function hukoomiPayment() {
    let itemId = JSON.parse(localStorage.getItem('request-details-data')).id;
    let serviceName = JSON.parse(localStorage.getItem('request-details-data')).name;
    let serviceId = '';
    if (serviceName === 'Certificate of Specification Amend') {
        serviceId = '1';
    } else if (serviceName === 'Transfer Ownership') {
        serviceId = '6';
    } else if (serviceName === 'Ship Deletion') {
        serviceId = '7';
    } else if (serviceName === 'Certificate of Non Encumbrance') {
        serviceId = '8';
    } else if (serviceName === 'Continuous Synopsis Record') {
        serviceId = '9';
    } else if (serviceName === 'Mortgage Registration') {
        serviceId = '10';
    } else if (serviceName === 'Non Encumbrance') {
        serviceId = '11';
    } else if (serviceName === 'Safe Manning') {
        serviceId = '12';
    } else if (serviceName === 'Derating Certificate') {
        serviceId = '13';
    } else if (serviceName === 'Exemption Certificate') {
        serviceId = '14';
    } else if (serviceName === 'Safety Insurance For Oil Pollution Damage') {
        serviceId = '15';
    } else if (serviceName === 'Extension Certificate') {
        serviceId = '17';
    } else if (serviceName === 'Lost Replacement') {
        serviceId = '18';
    } else if (serviceName === 'Marine Incident') {
        serviceId = '19';
    } else if (serviceName === 'To Whom It May Concern Certificate') {
        serviceId = '20';
    } else if (serviceName === 'NOL Request') {
        serviceId = '21';
    }

    let payload = {
        "itemID": itemId,
        "ServiceId": serviceId,
        "ServiceFees": "1" // $$('#serviceFee').text(),

    }
    getPaymentData(payload, function (data) {
        if (data.statusCode == 201) {
            r = data.Result[0];

            //            paymentUrl = "https://bveservices.motc.gov.qa/Style%20Library/maritime.aspx?transactionId=" + r.transactionId + "&hash=" + r.hash + "&amount=" + r.amount + "&confirmationURL=" + r.confirmationURL + "&eservice=" + r.eservice + "&serviceId=" + r.serviceId;

            paymentUrl = "pages/maritime.html?transactionId=" + r.transactionId + "&hash=" + r.hash + "&amount=" + r.amount + "&confirmationURL=" + r.confirmationURL + "&eservice=" + r.eservice + "&serviceId=" + r.serviceId;
            console.log(paymentUrl);
            vw = app.views.current;

            if (window.cordova) {
                var ref = cordova.InAppBrowser.open(paymentUrl, "_blank", "hidden=no,location=yes,clearsessioncache=yes,clearcache=yes,hideurlbar=yes,closebuttoncaption=Close,toolbarcolor=#8d1637,zoom=no,toolbar=yes,fullscreen=yes,footer=no,navigationbuttoncolor=#ffffff");
                ref.addEventListener('loadstart', loadStartCallBack);

                ref.addEventListener('loadstop', loadStopCallBack);

                ref.addEventListener('loaderror', loadErrorCallBack);




                function loadStartCallBack() {

                    console.log("start");
                }

                function loadStopCallBack(e) {
                    console.log(e);
                    url = e.url;


                    if (url.indexOf("PaymentMessage.aspx?PaymentStatus=0") > -1 || url.indexOf("PaymentMessage.aspx?PaymentStatus=1") > -1) {

                        console.log("i am here");
                        ref.close();
                        swal({
                            title: "Done!",
                            text: "Payment completed Successfully",
                            icon: "success"
                        }).then((value) => {
                            vw.router.refreshPage()
                        });




                    } else {
                        console.log("nope");
                    }

                }

                function loadErrorCallBack(params) {

                    console.log(params);
                    vw.router.refreshPage()


                }


            } else {
                window.open(paymentUrl, "_blank");

            }

            //$("#transactionId").val(r.transactionId);
            //$("#hash").val(r.hash);
        }
    })

    // $.ajax({
    //     type: 'post',
    //     url: SERVER_URL + '/GetPaymentData',
    //     data:
    //     {
    //         "itemID": itemId,
    //         "ServiceId": serviceId,
    //         "ServiceFees": $$('#serviceFee').text(),

    //     },
    //     success: function (data) {
    //         console.log("*******************************");
    //         if (data.statusCode == 201) {
    //             r = data.Result[0];

    //             //r.transactionId = 1367;
    //             //r.hash = '1FA7A0F68F228BCE4C6675A9D4F64ED531443456F8B05D9513AB61B5D726D978';
    //             //r.amount = '500';


    //             paymentUrl = "pages/maritime.html?transactionId=" + r.transactionId + "&hash=" + r.hash + "&amount=" + r.amount + "&confirmationURL=" + r.confirmationURL + "&eservice=" + r.eservice + "&serviceId=" + r.serviceId;
    //             console.log(paymentUrl);


    //             if (window.cordova) {
    //                 var ref = cordova.InAppBrowser.open(paymentUrl, "_blank", "hidden=no,location=yes,clearsessioncache=yes,clearcache=yes");
    //                 ref.addEventListener('loadstart', loadStartCallBack);

    //                 ref.addEventListener('loadstop', loadStopCallBack);

    //                 ref.addEventListener('loaderror', loadErrorCallBack);




    //                 function loadStartCallBack() {

    //                     console.log("start");
    //                 }

    //                 function loadStopCallBack() {
    //                     console.log("stop");

    //                 }

    //                 function loadErrorCallBack(params) {

    //                     console.log(params);

    //                 }




    //             } else {
    //                 window.open("/" + paymentUrl, "_self");

    //             }

    //             //$("#transactionId").val(r.transactionId);
    //             //$("#hash").val(r.hash);
    //         }
    //         console.log("*******************************");
    //     },
    //     error: function (err) {
    //         console.log(err);
    //     }
    // });
}

function showVesselDetails(e) {
    localStorage.setItem('selected-vessel-details-id', $$(e).children()[2].innerHTML);
    homeView.router.navigate('/vessel-details/');
}

function setOwnerTypeTransfer(type) {

    if (localStorage.getItem('isEdit') !== 'false') {
        if (type === 'company') {
            $$('#qid-buyer-attachement').hide();
            $$('#commercial-reg-buyer-attachement').show();
            $$('#transfer-qid-container').hide();
            $$('#transfer-cr-container').show();
            $$('#transfer-contact-container').show();
            $$("input[name='ownerType'][value='Company']").prop('checked', true);

        } else {
            $$('#commercial-reg-buyer-attachement').hide();
            $$('#qid-buyer-attachement').show();
            $$('#transfer-qid-container').show();
            $$('#transfer-cr-container').hide();
            $$('#transfer-contact-container').hide();
            $$("input[name='ownerType'][value='Individual']").prop('checked', true);
        }
    }
}

function setNewOrExistingOwner(owner) {
    ownerNewOrExistingBuyers = owner;
    if (owner === 'Existing') {
        $$('#buyerNew').hide();
        $$('#buyerExisting').show();
    } else {
        $$('#buyerNew').show();
        $$('#buyerExisting').hide();
    }
}

function selectOwner(id) {
    console.log($$('#SelectedOwnerId').val());

    getOwner({
        itemID: id
    }, function (ownerData) {
        // var ownerData = JSON.parse(ownerData);

        if (ownerData.Result[0].OwnerType === 'Company') {
            $$('#qid-buyer-attachement').hide();
            $$('#commercial-reg-buyer-attachement').show();
            $$('#transfer-qid-container').hide();
            $$('#transfer-cr-container').show();
            $$('#transfer-contact-container').show();
            $$("input[name='ownerType'][value='Company']").prop('checked', true);

        } else {
            $$('#commercial-reg-buyer-attachement').hide();
            $$('#qid-buyer-attachement').show();
            $$('#transfer-qid-container').show();
            $$('#transfer-cr-container').hide();
            $$('#transfer-contact-container').hide();
            $$("input[name='ownerType'][value='Individual']").prop('checked', true);
        }
    })
}

function selectCertificateType(serviceId) {
    console.log(typeof serviceId);
    if (replacementRequestStatus !== 'New' && replacementRequestStatus !== 'RequireUserUpdatesByNavigationSectionHead' && replacementRequestStatus !== 'RequireUserUpdatesByNavigationSectionEmployee') {
        return;
    }
    let fee = 0;
    if (serviceId == '2') {
        fee = (150 / 100) * 500;
        showLostSubmitBtn('true');
    } else if (serviceId == '3') {
        let vesselId = localStorage.getItem('vessel-selected-for-creating-new-request');
        getVesselDetails(vesselId, function (data) {

            fee = data.Result[0].Fees;
            $$('#serviceFee')[0].innerHTML = (150 / 100) * fee;
        })

        showLostSubmitBtn('true');
    } else if (serviceId == '5') {
        fee = (150 / 100) * 300;
        getCertificateList(function (vesselServicesData) {
            let services = JSON.parse(vesselServicesData).Result;
            services.forEach(function (service) {
                if (service.Name === 'Trading Certificate') {
                    let payload = {
                        ListGUID: service.ListGuid,
                        OwnerID: '3',
                        VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                    }
                    requestIsExist(payload, function (data) {

                        let isExist = (data).isExist;
                        if (isExist === 0) {
                            showLostSubmitBtn('false');
                            app.dialog.alert('You donot have any request for the selected service', 'Alert');
                        } else {
                            showLostSubmitBtn('true');
                        }
                    })
                }
            })
        })
    } else if (serviceId == '15') {
        fee = (150 / 100) * 500;
        getCertificateList(function (vesselServicesData) {
            let services = JSON.parse(vesselServicesData).Result;
            services.forEach(function (service) {
                if (service.Name === 'Safety Insurance For Oil Pollution Damage') {
                    let payload = {
                        ListGUID: service.ListGuid,
                        OwnerID: '3',
                        VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                    }
                    requestIsExist(payload, function (data) {

                        let isExist = (data).isExist;
                        if (isExist === 0) {
                            showLostSubmitBtn('false');
                            app.dialog.alert('You donot have any request for the selected service', 'Alert');
                        } else {
                            showLostSubmitBtn('true');
                        }
                    })
                }
            })
        })
    } else if (serviceId == '9') {
        fee = (150 / 100) * 500;
        getCertificateList(function (vesselServicesData) {
            let services = JSON.parse(vesselServicesData).Result;
            services.forEach(function (service) {
                if (service.Name === 'Continuous Synopsis Record') {
                    let payload = {
                        ListGUID: service.ListGuid,
                        OwnerID: '3',
                        VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                    }
                    requestIsExist(payload, function (data) {

                        let isExist = (data).isExist;
                        if (isExist === 0) {
                            showLostSubmitBtn('false');
                            app.dialog.alert('You donot have any request for the selected service', 'Alert');
                        } else {
                            showLostSubmitBtn('true');
                        }
                    })
                }
            })
        })
    } else if (serviceId == '14') {
        fee = (150 / 100) * 500;
        getCertificateList(function (vesselServicesData) {
            let services = JSON.parse(vesselServicesData).Result;
            services.forEach(function (service) {
                if (service.Name === 'Exemption Certificate') {
                    let payload = {
                        ListGUID: service.ListGuid,
                        OwnerID: '3',
                        VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                    }
                    requestIsExist(payload, function (data) {

                        let isExist = (data).isExist;
                        if (isExist === 0) {
                            showLostSubmitBtn('false');
                            app.dialog.alert('You donot have any request for the selected service', 'Alert');
                        } else {
                            showLostSubmitBtn('true');
                        }
                    })
                }
            })
        })
    } else if (serviceId == '17') {
        fee = (150 / 100) * 500;
        getCertificateList(function (vesselServicesData) {
            let services = JSON.parse(vesselServicesData).Result;
            services.forEach(function (service) {
                if (service.Name === 'Extension Certificate') {
                    let payload = {
                        ListGUID: service.ListGuid,
                        OwnerID: '3',
                        VesselID: localStorage.getItem('vessel-selected-for-creating-new-request').trim()
                    }
                    requestIsExist(payload, function (data) {

                        let isExist = (data).isExist;
                        if (isExist === 0) {
                            showLostSubmitBtn('false');
                            app.dialog.alert('You donot have any request for the selected service', 'Alert');
                        } else {
                            showLostSubmitBtn('true');
                        }
                    })
                }
            })
        })
    }
    $$('#serviceFee')[0].innerHTML = fee;
}

function showLostSubmitBtn(val) {
    if (val === 'true') {
        $$('#submit-lost').show();
        $$('#clear').show();
    } else {
        $$('#submit-lost').hide();
        $$('#clear').hide();
    }
}

function addNationality() {

    if ($$('#marineCraftName').val() === '' || $$('#marineCraftNationality').val() === '') {
        app.dialog.alert('Please enter name and nationality', 'Alert');
        return;
    }

    let nationalityObj = {
        Name: $$('#marineCraftName').val(),
        Nationality: $$('#marineCraftNationality').val()
    }
    nationalityArray.push(nationalityObj);
    nationalityArrayUpdate.push(nationalityObj);
    console.log(nationalityArray);

    $$('#marineCraftName').val('');
    $$('#marineCraftNationality').val('');

    var marineDetailsData = ''
    var marineDetails = marine_TPL({
        nationalityArray: nationalityArray
    });


    var t = nolMarineEvent.detail.$el[0];

    $$(t).find(".marine-craft-details").html(marineDetails);
}

var insurerArray = [];
function addInsurer() {
    let found = false;
    insurerData.insurer.forEach(function (v, i) {
        insurerArray.forEach(function (selected, i) {
            if (v.ID === selected.ID) {
                found = true;
            }
        })
    })
    if (found) {
        app.dialog.alert('This Insurer is already added', 'Alert');
    }
    else {
        let insurerObj = {
            ID: $$('#insurerName').val(),
            Name: "",
            Address: $$('#insurerAddressTextBox').val()
        }
        console.log(insurerData.insurer);
        insurerData.insurer.forEach(function (v, i) {
            if (v.ID.toString() === $$('#insurerName').val()) {
                insurerObj.Name = v.Name
            }
        })

        insurerArray.push(insurerObj);
        console.log(insurerArray);
        insurerData.insurerArray = insurerArray;
        var insurerDetails = insurer_TPL(insurerData);

        var t = insurerEvent.detail.$el[0];
        $$(t).find(".insurer_details").html(insurerDetails);
        $$('#insurerAddressTextBox').val(insurerData.insurer[0].Address);
    }
}

function setCompletionDate() {
    var now = moment($$('#CompletionDate')[0].value); //todays date
    var end = moment($$('#CommencedDate')[0].value); // another date
    var duration = moment.duration(now.diff(end));
    var days = duration.asDays();
    console.log(days);
    if (days < 0) {
        $$('#CompletionDate')[0].value = "";
        $$('#duration-of-work')[0].innerHTML = 0;
    } else {
        $$('#duration-of-work')[0].innerHTML = days + 1;
    }

}

function setCommencedDate() {
    var now = moment($$('#CompletionDate')[0].value); //todays date
    var end = moment($$('#CommencedDate')[0].value); // another date
    var duration = moment.duration(now.diff(end));
    var days = duration.asDays();
    console.log(days);
    if (days < 0) {
        $$('#CommencedDate')[0].value = "";
        $$('#duration-of-work')[0].innerHTML = 0;
    } else {
        $$('#duration-of-work')[0].innerHTML = days + 1;
    }
}

function selectInsurer(id) {
    console.log(id);

    insuranceGetItemByID({
        itemID: id
    }, function (data) {
        // let details = JSON.parse(data);
        let details = (data);
        $$('#insurerAddressTextBox').val(details.Result[0].Address);
    })
}

function checkEditMode(value) {
    console.log(value);
    isEdit = value;
}

function deleteAttachment(deleted, input, uploaded, fileType) {
    console.log(input);
    let payload = {
        FileType: fileType,
        FileName: $$('#' + input).val(),
        FileGUID: attachmentsGuid
    }
    console.log(payload);

    deleteFile(payload, function (data) {
        console.log(data);
        if (data.Result[0].ErrorIsExist == 0) {
            $$('#' + deleted).hide();
            $$('#' + input).val("");
            swal({
                title: "Done!",
                text: "Attachment deleted Successfully",
                icon: "success"
            })
        }
    })
}

var searchCriteriaValue = "ReqID"
function setSearchCriteria(value) {
    console.log(value);
    searchCriteriaValue = value
}

function a() {
    setTimeout(function () {
        console.log(">>>>>>>>>>>");
    }, 2000)
}

function searchRequest() {
    let value = $$('#requestSearchValue').val();

    setLanguage();

    if (value == '' || value == null) {
        let e = requestPageEvent;
        var Result = {
            Result: requestsAll
        };
        var vesselServices = myRequests_TPL(Result);

        var t = e.detail.$el[0];

        $$(t).find(".vessel-services-list").html(vesselServices);

        return;

    }

    localStorage.setItem('hideloader', false);
    app.preloader.show();
    let serviceCount = 0;



    var requestArray = [];

    getCertificateList(function (vesselServicesData) {

        localStorage.setItem('vesselServices', vesselServicesData);
        var vesselServicesData = JSON.parse(vesselServicesData);
        vesselServices = vesselServicesData;
        console.log(vesselServices);

        vesselServicesData.Result.forEach(function (element, i) {

            let requestData = {
                "OwnerID": '',
                "ListGUID": element.ListGuid,
                // searchCriteriaValue: value
                //"Name": $$(e).children()[1].innerHTML
            }
            requestData[searchCriteriaValue] = value;

            getAllRequestsByOwnerID(requestData, function (data) {

                var obj = {
                    Service: element.Name,
                    Requests: []
                }

                if (element.Name === 'Certificate of Specification Amend') {
                    obj.icon = "icon-application-for-certificate-of-qatar-registry- licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getSpecificationAmendDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Transfer Ownership') {
                    obj.icon = "icon-application-of-ownership-transfer licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getOwnershipTransferDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })


                } else if (element.Name === 'Ship Deletion') {
                    obj.icon = "icon-issuing-vessel-deletion-certificate licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getShipDeletionDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Certificate of Non Encumbrance') {
                    obj.icon = "icon-issuing-a-certificate-of-non-encumbrance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNonEncumbranceCertDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Continuous Synopsis Record') {
                    obj.icon = "icon-issuing-continuous-synopsis-record licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getCSRDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Mortgage Registration') {
                    obj.icon = "icon-application-of-mortgage-registration licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getMortgageDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Non Encumbrance') {
                    obj.icon = "icon-application-of-non-encumbrance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNonEncumbranceDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Safe Manning') {
                    obj.icon = "icon-safe-manning-certificate licon"
                } else if (element.Name === 'Derating Certificate') {
                    obj.icon = "icon-derating-certificate licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getDerratingDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Exemption Certificate') {
                    obj.icon = "icon-issuing-exemption-certificates licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getExemptionDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Safety Insurance For Oil Pollution Damage') {
                    obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getSafetyInsuranceDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Extension Certificate') {
                    obj.icon = "icon-issuing-a-certificate-of-authorization licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getExtensionDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Lost Replacement') {
                    obj.icon = "icon-application-for-issue-the-certificate-of licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getLostAndReplacementDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Marine Incident') {
                    obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getMarineIncidentDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'To Whom It May Concern Certificate') {
                    obj.icon = "icon-issuing-a-certificate-to-whom-it licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getToWhomDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'NOL Request') {
                    obj.icon = "icon-issuing-nol-for-the-announcement-of- licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNOLDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            detail.Result[0].Service = element.Name;
                            obj.Requests.push(detail.Result[0]);
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                }

                if (element.Name !== 'Safe Manning' && element.Name !== 'Provisional Registery' && element.Name !== 'Permenant Registery' && element.Name !== 'Trading Certificate' && element.Name !== 'Annual Fee') {
                    requestArray.push(obj);
                }


                console.log(vesselServicesData.Result.length);
                if (vesselServicesData.Result.length === i + 1) {
                    allRequests = requestArray
                    let interval = true;
                    setTimeout(function () {
                        if (serviceCount === 14 && interval) {
                            interval = false;
                            showRequests(requestArray, requestPageEvent, '');
                        }
                    }, 6000)
                }

            })
        }, this);



    })


    setTimeout(function () {
        localStorage.setItem('hideloader', true);
        app.preloader.hide();
    }, 7000);


}

function searchTasks() {
    let value = $$('#taskSearchValue').val();
    let array = [];

    setLanguage();

    if (value == '' || value == null) {
        let e = requestPageEvent;
        var Result = {
            Result: tasksAll
        };
        var vesselServices = tasksList_TPL(Result);

        var t = e.detail.$el[0];

        $$(t).find(".tasks-list").html(vesselServices);

        return;

    }


    let owner = JSON.parse(localStorage.getItem('owner-data')).Result[0];
    localStorage.setItem('hideloader', false);
    app.preloader.show();
    let serviceCount = 0;


    var searchbar = app.searchbar.create({
        // el: '#taskSearch',
        // searchContainer: '#tasksList',
        // searchIn: '.item-title',
        // on: {
        //     search(sb, query, previousQuery) {
        //         console.log(query, previousQuery);
        //     }
        // }
    });

    var requestArray = [];
    
    getCertificateList(function (vesselServicesData) {

        localStorage.setItem('vesselServices', vesselServicesData);
        var vesselServicesData = JSON.parse(vesselServicesData);
        vesselServices = vesselServicesData;
        console.log(vesselServices);

        vesselServicesData.Result.forEach(function (element, i) {

            let requestData = {
                "OwnerID": owner.ID,
                "ListGUID": element.ListGuid,
                //"Name": $$(e).children()[1].innerHTML
            }
            requestData[searchCriteriaValue] = value;

            getAllRequestsByOwnerID(requestData, function (data) {
                var obj = {
                    Service: element.Name,
                    Requests: []
                }

                if (element.Name === 'Lost Replacement') {
                    obj.icon = "icon-application-for-issue-the-certificate-of licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getLostAndReplacementDetails(request.ID, function (detail) {
                            // let detail = JSON.parse(details);
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }

                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Marine Incident') {
                    obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getMarineIncidentDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'To Whom It May Concern Certificate') {
                    obj.icon = "icon-issuing-a-certificate-to-whom-it licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getToWhomDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'NOL Request') {
                    obj.icon = "icon-issuing-nol-for-the-announcement-of- licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNOLDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Certificate of Specification Amend') {
                    obj.icon = "icon-application-for-certificate-of-qatar-registry- licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getSpecificationAmendDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Transfer Ownership') {
                    obj.icon = "icon-application-of-ownership-transfer licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getOwnershipTransferDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })


                } else if (element.Name === 'Ship Deletion') {
                    obj.icon = "icon-issuing-vessel-deletion-certificate licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getShipDeletionDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Certificate of Non Encumbrance') {
                    obj.icon = "icon-issuing-a-certificate-of-non-encumbrance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNonEncumbranceCertDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Continuous Synopsis Record') {
                    obj.icon = "icon-issuing-continuous-synopsis-record licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getCSRDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Mortgage Registration') {
                    obj.icon = "icon-application-of-mortgage-registration licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getMortgageDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Non Encumbrance') {
                    obj.icon = "icon-application-of-non-encumbrance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getNonEncumbranceDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Safe Manning') {
                    obj.icon = "icon-safe-manning-certificate licon"
                } else if (element.Name === 'Derating Certificate') {
                    obj.icon = "icon-derating-certificate licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getDerratingDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Exemption Certificate') {
                    obj.icon = "icon-issuing-exemption-certificates licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getExemptionDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Safety Insurance For Oil Pollution Damage') {
                    obj.icon = "icon-issuing-a-certificate-of-safety-insurance licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getSafetyInsuranceDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                } else if (element.Name === 'Extension Certificate') {
                    obj.icon = "icon-issuing-a-certificate-of-authorization licon"

                    if (data.Result.length === 0) {
                        serviceCount += 1;
                    }

                    data.Result.forEach(function (request, i) {
                        getExtensionDetails(request.ID, function (detail) {
                            if (detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionHead' ||
                                detail.Result[0].RequestStatus === 'RequireUserUpdatesByNavigationSectionEmployee' ||
                                detail.Result[0].RequestStatus === 'PendingOnVesselOwnerToFixInspectionProblems' ||
                                detail.Result[0].RequestStatus === 'PendingOnPayment') {
                                detail.Result[0].Service = element.Name;
                                obj.Requests.push(detail.Result[0]);
                            }
                            else { }
                        })
                        if (i + 1 === data.Result.length) {
                            serviceCount += 1;
                        }
                    })
                }
                if (element.Name !== 'Safe Manning' && element.Name !== 'Provisional Registery' && element.Name !== 'Permenant Registery' && element.Name !== 'Trading Certificate' && element.Name !== 'Annual Fee') {
                    requestArray.push(obj);
                }


                console.log(vesselServicesData.Result.length);
                if (vesselServicesData.Result.length === i + 1) {
                    allRequests = requestArray
                    let interval = true;
                    setTimeout(function () {
                        if (serviceCount === 15 && interval) {
                            interval = false;

                            requestArray.forEach(function (v, i) {
                                if (v.Requests.length != 0) {
                                    array.push(v);
                                }
                            })

                            var Result = {
                                Result: array
                            };

                            var vesselServices = tasksList_TPL(Result);

                            var t = taskPageEvent.detail.$el[0];

                            $$(t).find(".tasks-list").html(vesselServices);
                        }
                    }, 5000)
                }

            })
        }, this);
    })
    setTimeout(function () {
        localStorage.setItem('hideloader', true);
        app.preloader.hide();
    }, 7000)
}

function search() {
    let searchValue = $$('#search-criteria').val();
    let requestList = JSON.parse(JSON.stringify(allRequests));
    console.log($$('#search-criteria').val());
    console.log(allRequests);
    if (searchValue === '') {
        console.log('working');
        showRequests(allRequests, requestPageEvent);
    } else {
        let found = false;
        let result = {};
        requestList.forEach(function (service) {
            service.Requests.forEach(function (request) {
                if (request.ID === Number(searchValue)) {
                    console.log(request.Name);
                    found = true;
                    result = {
                        Service: service.Service,
                        Requests: [request],
                        icon: service.icon
                    }
                }
            })
        })

        if (found) {
            console.log(result);
            showRequests([result], requestPageEvent);
        } else {
            showRequests([], requestPageEvent);
        }
    }

}

function addMultipeAttachment () {
    $$('.multi-attach').append("<div class='row'> <div class='col-100'> <div class='item-inner upload-document-input a'> <div class='item-input-wrap'> <input id='derating-attachment-rodent' type='text' readonly> <i class='bv upload-input-icon icon-upload-document-01 icon-down-arrow'></i> <input onchange='checkFile('derating-attachment-rodent', 'derating-attachment-input-rodent', 'Copy Of The Rodent Fee Test Report')' type='file' id='derating-attachment-input-rodent'/> </div> </div> </div> </div>")
}