routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/home/',
    url: './pages/home.html',
  },

  {
    path: '/profile/',
    // async(routeTo, routeFrom, resolve, reject) {
    //   console.log(routeTo);
    //   if (localStorage.getItem('nas-user-data')) {
    //     resolve({
    //       url: './pages/profile.html',
    //     });
    //     }
    //   else {
    //     resolve({
    //       url: './pages/login.html',
    //     });
    //         }
    
      
    // },
            url: './pages/profile.html',

  },

  {
    path: '/certificates/',
    url: './pages/bv/certificates.html',
  },
  {
    path: '/login/',
    async(routeTo, routeFrom, resolve, reject) {

      if (localStorage.getItem('nas-user-data')) {
        if (routeFrom.path === '/otp/') {
          resolve({ url: './pages/maritime-services.html', path: '/maritime-services/' })
        }
        else {
          resolve({ path: '/home-bgv/', url: './pages/home-bgv.html' })
        }
        //resolve({ url: './pages/otp.html' })
      } else {
        resolve({ url: './pages/login.html', path: '/login/' })
      }
    }
  },
  {
    path: '/forgot/',
    url: './pages/forgot.html',
  },
  {
    path: '/home-bgv/',
    url: './pages/bv/home-bgv.html',
  },
  {
    path: '/my-vessels/',
    url: './pages/bv/my-vessels.html',
  },
  {
    path: '/my-details/',
    url: './pages/bv/my-details.html',
  },
  {
    path: '/derrating-certificate/',
    url: './pages/bv/certificate-derrating.html',
  },
  {
    path: '/safe-manning-certificate/',
    url: './pages/bv/certificate-safe-manning.html',
  },
  {
    path: '/non-encumbrance-certificate/',
    url: './pages/bv/certificate-non-encumbrance.html',
  },
  {
    path: '/marine-incident-certificate/',
    url: './pages/bv/certificate-marine-incident.html',
  },
  {
    path: '/issue-non-encumbrance-certificate/',
    url: './pages/bv/certificate-non-encumbrance-issue.html',
  },
  {
    path: '/issue-exemption-certificates/',
    url: './pages/bv/certificate-exemption-issue.html',
  },
  {
    path: '/safety-insurance-certificate/',
    url: './pages/bv/certificate-safety-insurance.html',
  },
  {
    path: '/issue-NOL-certificate/',
    url: './pages/bv/certificate-NOL.html',
  },
  {
    path: '/issue-towhomconcern-certificate/',
    url: './pages/bv/certificate-to-whom-concern.html',
  },
  {
    path: '/lost-replacement-certificate/',
    url: './pages/bv/certificate-lost-replacement.html',
  },
  {
    path: '/csr-certificate/',
    url: './pages/bv/certificate-csr.html',
  },
  {
    path: '/mortgage-request/',
    url: './pages/bv/request-mortgage.html',
  },
  {
    path: '/issue-vessel-deletion-certificate-request/',
    url: './pages/bv/request-vessel-deletion-certificate.html',
  },
  {
    path: '/ownership-transfer-request/',
    url: './pages/bv/request-ownership-transfer.html',
  },
  {
    path: '/certificate-of-specification-amend-request/',
    url: './pages/bv/request-specification-amend.html',
  },
  {
    path: '/issue-authorization-extension-relaxation-certificate/',
    url: './pages/bv/certificate-authorization-extension-relaxation.html',
  },
  {
    path: '/requests/',
    url: './pages/bv/Requests.html',
  },
  {
    path: '/owner-requests/',
    url: './pages/bv/requests-owner.html',
  },
  {
    path: '/vessels-certificate/',
    url: './pages/bv/vessels-certificate.html',
  },
  {
    path: '/vessel-details/:vesselID',
    url: './pages/bv/vessel-details.html',
  },
  {
    path: '/tasks-list/',
    url: './pages/bv/task-list.html',
  },
  {
    path: '/videos/',
    componentUrl: './pages/videos.html',
  },
  {
    path: '/tenders/',
    componentUrl: './pages/tenders.html',
  },
  {
    path: '/news/',
    componentUrl: './pages/news.html',
  },
  {
    path: '/events/',
    componentUrl: './pages/events.html',
  },
  {
    path: '/maritime/',
    url: './pages/maritime.html',
  },
  {
    path: '/about-us/',
    url: './pages/about-us.html',
  },
  {
    path: '/services/',
    url: './pages/services.html',
  },
  {
    path: '/contact/',
    url: './pages/contact.html',
  },
  {
    path: '/maritime-services/',
    url: './pages/maritime-services.html',
  },
  {
    path: '/sectors/',
    url: './pages/sectors.html',
  },
  {
    path: '/otp/',
    // async(routeTo, routeFrom, resolve, reject) {
    //   console.log(routeFrom)
    //   console.log(routeTo)
    //   if (localStorage.getItem('nas-user-data')) {

    //     // resolve({path: '/'})
    //     //resolve({ url: './pages/maritime-services.html' })
    //     if(localStorage.getItem('one-time-login')) {
    //       resolve({ url: './pages/maritime-services.html' })
    //     }
    //     else {
    //       reject();
    //     }

    //   } else {
    //     resolve({ url: './pages/otp.html' })
    //   }
    // }
    url: './pages/otp.html'
  },
  {
    path: '/notification/',
    url: './pages/notification.html',
  },

  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    async(routeTo, routeFrom, resolve, reject) {
      resolve({ url: './index.html', path: '/' })
      setLanguage();

    }
  },
];
