
function getVideos(lang, itemsPerPage, callback) {
   
    hf.postData(false, lang + "/api/1.0/video?items_per_page=" + itemsPerPage, function (data) {
        callback(data);
    });
}

function getTenders(lang, itemsPerPage, callback) {
   
    hf.postData(false, lang + "/api/1.0/announcements?items_per_page=" + itemsPerPage, function (data) {
        callback(data);
    });
}

function getNews(lang, itemsPerPage, callback) {
   
    hf.postData(false, lang + "/api/1.0/news?items_per_page=" + itemsPerPage, function (data) {
        callback(data);
    });
}

function getEvents(lang, itemsPerPage, callback) {
   
    hf.postData(false, lang + "/api/1.0/event?items_per_page=" + itemsPerPage, function (data) {
        callback(data);
    });
}

function loginUser(payload, callback) {
    let params = payload
    console.log(params);
    hf.postData('login', "otpAuthenticate", payload, function (data) {
        callback(data);
    });
}

function validateUser(payload, callback) {
   
    hf.postData('login', "otpValidate", payload, function (data) {
        callback(data);
    });
}