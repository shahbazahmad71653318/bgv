// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app = new Framework7({
  root: '#app', // App root element
  id: 'io.cordova.bgv', // App bundle ID
  name: 'Big Vessel', // App name
  theme: 'ios', // Automatic theme detection
  lazy: {
    threshold: 50,
    sequential: true,
  },
  methods: {},
  // App routes
  pushState: false,
  pushStateAnimate: true,
  routes: routes,
  swipePanel: 'left',
  initOnDeviceReady: true
});


// Init/Create views
var homeView = app.views.create('#view-home', {
  url: '/home/',
  animate: false
});


var profileView = app.views.create('#view-profile', {
  url: '/profile/'
});

var settingsView = app.views.create('#view-settings', {
  url: '/notification/'
});

app.views.get();

var token;
var isEdit = false;
var ref;
var checkReadmore = false;


function openLeftPanel() {
  app.panel.open('left', true);
}

function openRightPanel() {
  app.panel.open('right', true);
}


Template7.registerHelper('dt', function (dt) {
  return moment(dt).format('DD-MM-YYYY');
});

function goToBGV() {
  homeView.router.load("/home-bgv/");
}

function login() {
  // let username =  "tabishikram1947@gmail.com";
  // let password = "Master@123";
  //let username = "28081810315";
  //let password = "Mini1234*"
  let username = $$('#loginUsername').val();
  let password = $$('#loginPassword').val();

  if (!username || username === null) {
    app.dialog.alert('Please enter your username', 'Alert');
    return;
  }
  if (!password || password === null) {
    app.dialog.alert('Please enter password', 'Alert');
    return;
  }

  let payload = "{uID:'" + username + "', uPWD:'" + password + "'}"
  console.log(payload);
  localStorage.setItem('hideloader', false);
  app.preloader.show();
  loginUser(payload, function (data) {
    let response = {};
    var data = JSON.parse(data.d);
    console.log(data);
    for (var key in data) {
      if (key == "transactionIdField" && data[key] != null) {
        response.transactionId = data[key];
      } else if (key == "transportKeyField" && data[key] != null) {
        response.transportKey = data[key];
      } else if (key == "errorCodeField" && data[key] != null) {
        response.errorCode = data[key];
      } else if (key == "errorDescField" && data[key] != null) {
        response.errorDescription = data[key];
      }
    }
    console.log(response);
    app.preloader.hide();
    localStorage.setItem('hideloader', true);
    if (response.hasOwnProperty('transactionId')) {
      console.log('yes');
      localStorage.setItem('userCredentials', JSON.stringify(response));
      //     currentRouter = app.views.current.router;
      //      currentRouter.navigate("/otp/");

      $$("#loginForm").hide();
      $$("#otpForm").show();

    } else {
      app.dialog.alert(response.errorDescription, 'Alert');
    }

  })
}

function OTP() {
  let otp = $$('#loginOTP').val();
  if (!otp) {
    app.dialog.alert('Please enter the otp', 'Alert');
    return;
  }
  let userCredentials = JSON.parse(localStorage.getItem('userCredentials'));
  let response = {};
  let payload = "{OTP:'" + otp + "',TransactionId:'" + userCredentials.transactionId + "', TransportKey:'" + userCredentials.transportKey + "'}";

  localStorage.setItem('hideloader', false);
  app.preloader.show();
  validateUser(payload, function (data) {
    console.log(data);
    var token = data.d.token;
    var data = data.d;
    console.log(data);

    if (typeof data === 'string') {
      var data = JSON.parse(data);
    }

    for (var key in data) {
      if (key == "userEmailField" && data[key] != null) {
        response.userEmailField = data[key];
      } else if (key == "errorCodeField" && data[key] != null) {
        response.errorCode = data[key];
      } else if (key == "errorDescField" && data[key] != null) {
        response.errorDescription = data[key];
      }
    }
    console.log(data);
    app.preloader.hide();
    localStorage.setItem('hideloader', true);
    if (response.hasOwnProperty('userEmailField')) {

      localStorage.setItem('nas-user-data', JSON.stringify(data));
      $$('.logout-menu-item').show();
      $$('.login-menu-item').hide();

      Framework7.request.setup({
        headers: {
          'Authorization': 'Bearer ' + token
        }
      })


      localStorage.setItem('one-time-login', true);
      loginPopup.close();
      currentRouter = app.views.current.router;
      $$('#loginOTP').val("");
      currentRouter.navigate("/home-bgv/");


      showBottomToast('You are successfully logged in');
    } else if (response.hasOwnProperty('errorDescription')) {
      app.dialog.alert(response.errorDescription, 'Alert');
    }


  })

}

function checkProfileView() {
  userData = localStorage.getItem("nas-user-data");

  if (userData) {
    userData = JSON.parse(userData);

    $$('#profile_email').text(userData.userEmailField);
    $$('#profile_number').text(userData.userMobileNumberField);
    name = "-";
    if (userData.userFirstNameENField && userData.userMiddleNameENField) {
      name = userData.userFirstNameENField + " " + userData.userMiddleNameENField;
    } else {
      name = userData.userFirstNameARField + " " + userData.userMiddleNameARField;
    }

    $$('#profile_name').text(name);
    $$('#profile_fullname').text(name);

    getBadgesCountersByOwnerID(function (data) {
      $$('#request-count').text(data.RequestsCount);
      $$('#task-count').text(data.TasksCount);
      $$('#vessel-count').text(data.VesselsCount);



    })
  }

}



if (localStorage.getItem('language') === 'English') {
  $$("input[name='lang-switch']").prop('checked', true);
} else {
  $$("input[name='lang-switch']").prop('checked', false);
}
if ($$("input[name='lang-switch']:checked").val() === 'on') {
  console.log('on');
  localStorage.setItem('language', 'English');
}


$$(document).on('click', '#recoverbtn', function (e) {
  var username = $$('#loginUsername').val();
  var password = $$('#loginPassword').val();

})

// Dialog Box Function
function showToast(text) {
  console.log(text);
  // var toastWithButton = app.toast.create({
  //   text: text,
  //   closeButton: true,
  //   closeTimeout: 4000,
  //   position: 'bottom',
  // });
  // toastWithButton.open();
  swal({
    title: "Done!",
    text: text,
    icon: "success",
  });
}

// Bottom Toast Function
function showBottomToast(text) {
  console.log(text);
  var toastWithButton = app.toast.create({
    text: text,
    closeButton: true,
    closeTimeout: 4000,
    position: 'bottom',
  });
  toastWithButton.open();
}

app.on('accordionOpen', function (el) {
  var a = $$(el).children().children().children().children()[2];
  var title = $$(el).children().children().children().children()[1];
  console.log($$(el).children().children().children().children()[1]);
  $$(a).removeClass('icon-arrow-down');
  $$(a).addClass('icon-arrow-up');

  $$(title).removeClass('color-black');
  $$(title).addClass('color-red');
});

app.on('accordionClosed', function (el) {
  var a = $$(el).children().children().children().children()[2];
  var title = $$(el).children().children().children().children()[1];
  $$(a).removeClass('icon-arrow-up');
  $$(a).addClass('icon-arrow-down');

  $$(title).removeClass('color-red');
  $$(title).addClass('color-black');
});

$$(document).on('page:init', '.page[data-name="about-us"]', function (e) {
  setLanguage();



})

$$(document).on('page:init', '.page[data-name="sectors"]', function (e) {
  setLanguage();

})

$$(document).on('page:init', '.page[data-name="services"]', function (e) {

  setLanguage();


})

$$(document).on('page:init', '.page[data-name="maritime-services"]', function (e) {

  setLanguage();


})

$$(document).on('page:init', '.page[data-name="contact-us"]', function (e) {

  setLanguage();


  $$('.contact-phone').attr({
    dir: 'ltr',
    lang: 'en'
  });


})

$$(document).on('page:init', '.page[data-name="notification"]', function (e) {
  setLanguage();
})

$$(document).on('page:init', '.page[data-name="profile"]', function (e) {
  setLanguage();
})

$$(document).on('page:init', '.page[data-name="home"]', function (e) {
  initializeApp();
})

// Videos
$$(document).on('page:init', '.page[data-name="videos"]', function (e) {

  setLanguage();

});

function closeSideMenu() {
  app.panel.close();
}

function initializeApp() {

  console.log("--------------------------");
  if (window.cordova) {

  }

  if (localStorage.getItem('language') === null) {
    localStorage.setItem('language', 'English');
  }


  if (localStorage.getItem('language') === 'Arabic') {

    $$('.text').addClass('align-right');
    $$('.text').attr({
      dir: 'rtl',
      lang: 'ar'
    });

    $$('.Ara').show();
    $$('.Eng').hide();
    $$('#lang-btn1')[0].innerHTML = 'En';
    $$('#lang-btn2')[0].innerHTML = 'En';

    $$('.profile-class').addClass('profile-circle-ar');
    $$('.profile-ar').addClass('profile-media-ar');
    $$('.menu-ar').addClass('menu-media-ar');


  } else {
    $$('.Eng').show();
    $$('.Ara').hide();
    $$('#lang-btn1')[0].innerHTML = 'ع';
    $$('#lang-btn2')[0].innerHTML = 'ع';

    $$('.text').removeClass('align-right');
    $$('.text').attr({
      dir: 'ltr',
      lang: 'en'
    });

    $$('.profile-class').removeClass('profile-circle-ar');
    $$('.profile-ar').removeClass('profile-media-ar');
    $$('.menu-ar').removeClass('menu-media-ar');

  }
  $$('.logout-menu-item').hide();
}

function changeLanguage(lang) {

  if (localStorage.getItem('language') === 'Arabic') {

    localStorage.setItem('language', 'English');

    $$('.Eng').show();
    $$('.Ara').hide();

    $$('#lang-btn2')[0].innerHTML = 'ع';
    $$('#lang-btn1')[0].innerHTML = 'ع';

    $$('.text').removeClass('align-right');
    $$('.text').attr({
      dir: 'ltr',
      lang: 'en'
    });

    $$('.profile-class').removeClass('profile-circle-ar');
    $$('.profile-ar').removeClass('profile-media-ar');
    $$('.menu-ar').removeClass('menu-media-ar');
  } else {
    console.log('off');
    localStorage.setItem('language', 'Arabic');
    $$('.text').addClass('align-right');
    $$('.text').attr({
      dir: 'rtl',
      lang: 'ar'
    });
    $$('.Ara').show();
    $$('.Eng').hide();
    $$('#lang-btn2')[0].innerHTML = 'EN';
    $$('#lang-btn1')[0].innerHTML = 'EN';

    $$('.profile-class').addClass('profile-circle-ar');
    $$('.profile-ar').addClass('profile-media-ar');
    $$('.menu-ar').addClass('menu-media-ar');
  }

}

function goBack() {
  homeView.router.back();
  refreshLanguageBack();
}

function goToLogin() {
  if (localStorage.getItem('nas-user-data')) {
    homeView.router.navigate('/home-bgv/');
  } else {
    loginPopup.open();
  }
}

function goToMaritime() {
  if (localStorage.getItem('one-time-login')) {
    homeView.router.back();
    homeView.router.back();
  }

}

function refreshLanguage() {
  if (localStorage.getItem('language') === null) {
    localStorage.setItem('language', 'English');
  }


  setTimeout(function () {
    if (localStorage.getItem('language') === 'Arabic') {

      $$('.text').addClass('align-right');
      $$('.text').attr({
        dir: 'rtl',
        lang: 'ar'
      });

      $$('.Ara').show();
      $$('.Eng').hide();
      $$('#lang-btn2')[0].innerHTML = 'EN';
      $$('#lang-btn1')[0].innerHTML = 'EN';

      $$('.profile-class').addClass('profile-circle-ar');
      $$('.profile-ar').addClass('profile-media-ar');
      $$('.menu-ar').addClass('menu-media-ar');


    } else {
      $$('.Eng').show();
      $$('.Ara').hide();

      setTimeout(function () {


      }, 1000)

      $$('#lang-btn2')[0].innerHTML = 'ع';
      $$('#lang-btn1')[0].innerHTML = 'ع';

      $$('.text').removeClass('align-right');
      $$('.text').attr({
        dir: 'ltr',
        lang: 'en'
      });

      $$('.profile-class').removeClass('profile-circle-ar');
      $$('.profile-ar').removeClass('profile-media-ar');
      $$('.menu-ar').removeClass('menu-media-ar');

    }
  }, 100)
  homeView.router.navigate('/home/');
}


function refreshLanguageBack() {

  if (localStorage.getItem('language') === null) {
    localStorage.setItem('language', 'English');
  }

  setTimeout(function () {
    if (localStorage.getItem('language') === 'Arabic') {

      $$('.text').addClass('align-right');
      $$('.text').attr({
        dir: 'rtl',
        lang: 'ar'
      });

      $$('.Ara').show();
      $$('.Eng').hide();
      $$('#lang-btn2')[0].innerHTML = 'EN';
      $$('#lang-btn1')[0].innerHTML = 'EN';

      $$('.profile-class').addClass('profile-circle-ar');
      $$('.profile-ar').addClass('profile-media-ar');
      $$('.menu-ar').addClass('menu-media-ar');


    } else {
      $$('.Eng').show();
      $$('.Ara').hide();

      setTimeout(function () {


      }, 1000)

      $$('#lang-btn2')[0].innerHTML = 'ع';
      $$('#lang-btn1')[0].innerHTML = 'ع';

      $$('.text').removeClass('align-right');
      $$('.text').attr({
        dir: 'ltr',
        lang: 'en'
      });

      $$('.profile-class').removeClass('profile-circle-ar');
      $$('.profile-ar').removeClass('profile-media-ar');
      $$('.menu-ar').removeClass('menu-media-ar');

    }
  }, 100)
  homeView.router.navigate('/');
}

function loginFromMainMenu() {

  if (localStorage.getItem('nas-user-data')) {
    app.dialog.alert('You are already logged in', 'Alert');
  } else {
    localStorage.setItem('login-main-menu', true);
    homeView.router.navigate('/login/');
  }


}

function logout() {

  if (localStorage.getItem('nas-user-data') == null) {
    app.dialog.alert('You are already logged out!', 'Alert');
  } else {
    localStorage.removeItem('nas-user-data');
    homeView.router.navigate('/home/');
    refreshLanguage();
    showBottomToast('You are logged out');
  }

  $$('.logout-menu-item').hide();
  $$('.login-menu-item').show();


}

function openSocialMedia(link) {
  if (link === 'facebook') {
    cordova.InAppBrowser.open('fb://page/53594218718', '_system', 'location=yes');
  }
  if (link === 'instagram') {
    cordova.InAppBrowser.open('https://www.instagram.com/motc_qatar', '_system', 'location=yes');
  }
  if (link === 'twitter') {
    cordova.InAppBrowser.open('https://twitter.com/MOTC_QA', '_system', 'location=no');
  }
  if (link === 'youtube') {
    cordova.InAppBrowser.open('https://www.youtube.com/channel/UCaCew7yEVHcd1gfkuQrEaLw', '_system', 'location=no');
  }
  if (link === 'linkedin') {
    cordova.InAppBrowser.open('https://www.linkedin.com/company/ministry-of-transport-&-communication', '_system', 'location=no');
  }
}

function loadStartCallBack() {
  app.preloader.show();
}

function loadStopCallBack() {

  if (ref != undefined) {

    app.preloader.hide();

    ref.show();
  }

}

function readmore(no) {
  checkReadmore = true
}

function handleReadMoreNews(check) {
  setTimeout(function () {
    if (checkReadmore) {
      $$($$(check)[0].children[2]).addClass('height');
      $$($$(check)[0].children[3]).hide();
      $$($$(check)[0].children[4]).hide();
      checkReadmore = false;
    }
  }, 100)
}

function loadNewsFromWeb() {
  var ref;
  var link;

  if (localStorage.getItem('language') === 'Arabic') {
    link = 'http://www.motc.gov.qa/ar/media-center/news';
  } else {
    link = 'http://www.motc.gov.qa/en/media-center/news';
  }
  ref = cordova.InAppBrowser.open(link, '_blank', 'location=no');


  ref.show();
}


function setLanguage() {

  let lang = localStorage.getItem('language');

  if (lang === 'Arabic') {

    $$('.text').addClass('align-right');
    $$('.text').attr({
      dir: 'rtl',
      lang: 'ar'
    });

    $$('.Ara').show();
    $$('.Eng').hide();
  } else {
    $$('.text').removeClass('align-right');
    $$('.text').attr({
      dir: 'ltr',
      lang: 'en'
    });
    $$('.Eng').show();
    $$('.Ara').hide();

  }
}

function adjustLangLayout() {
  lang = localStorage.getItem('language');

  if (lang == 'English') {
    $$('.text').removeClass('align-right');
    $$('.text').attr({
      dir: 'ltr',
      lang: 'en'
    });
    $$('.Eng').show();
    $$('.Ara').hide();
  } else {


    $$('.text').addClass('align-right');
    $$('.text').attr({
      dir: 'rtl',
      lang: 'ar'
    });

    $$('.Ara').show();
    $$('.Eng').hide();
  }
}

function getLanguage() {

  language = localStorage.getItem('language');
  if (language) {
    return language;
  } else {
    return 'English';
  }
}

function onDeviceReady() {
  console.log("device ready");
  // console.log(device.cordova);
  navigator.splashscreen.hide();
  if (localStorage.getItem('nas-user-data') !== null) {
    localStorage.removeItem('nas-user-data');

  }

  document.addEventListener('backbutton', onBackKeyDown, false);
}

function onBackKeyDown(event) {
  event.preventDefault();
  var vw = app.views.current;
  console.log(vw.history);
  if (loginPopup.opened) {
    loginPopup.close();
  } else if (vw.history.length == 1 && vw.main === true) {




    if (localStorage.getItem("language") == "Arabic") {

      navigator.notification.confirm("هل أنت متأكد أنك تريد الخروج ؟  ", handleExitConfirmation, "MOTC", ["لا", "نعم"]);
    } else {

      navigator.notification.confirm("Are you sure you want exit?", handleExitConfirmation, "MOTC", ["No", "Yes"]);
    }


    //  

    function handleExitConfirmation(button) {

      console.log(button);

      if (button == 2) {
        navigator.app.exitApp();
      }

    }



  } else {
    vw.router.back();

  }
}



var loginPopup = app.popup.create({
  content: $$(".loginpopup")
});
loginPopup.on('open', function (popup) {
  $$("#loginForm").show();
  $$("#otpForm").hide();


  if (window.cordova) {
    SMSReceive.startWatch(function () {
      console.log('smsreceive: watching started');


      document.addEventListener('onSMSArrive', function (e) {
        var IncomingSMS = e.data;

        if (IncomingSMS.address == "Tawtheeq") {
          otpCode = IncomingSMS.body;
          otpCode = otpCode.replace("Please use the following code: ", "")
          otpCode = otpCode.replace(". Thank you!", "");
          otpCode = otpCode.replace("الرجاء استخدام الرمز التالي: ", "");
          otpCode = otpCode.replace(". شكرًا لكم!", "");


          if (parseInt(otpCode)) {
            $$("#loginOTP").val(otpCode);
            OTP();

            SMSReceive.stopWatch(function () {
              console.log('smsreceive: watching stopped');
            }, function () {
              console.warn('smsreceive: failed to stop watching');
            });


          }
        }
      });


    }, function () {
      console.warn('smsreceive: failed to start watching');
    });
  }



});


function gotoHome() {

  currentRouter = app.views.current.router;
  currentRouter.navigate("/home/", {
    clearPreviousHistory: true
  });

}

document.addEventListener("deviceready", onDeviceReady, false);