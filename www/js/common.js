// var SERVER_URL = "https://bvservices.malomatia.com/";
var SERVER_URL = "https://api.motc.gov.qa/"
// var SERVER_URL = "https://api.motc.gov.qa/";
var SERVER_URL_2 = "http://motc.gov.qa/";
// var SERVER_URL_AUTH = "http://172.30.23.236:80/MOTCJSONWS/GOVJSONWS.asmx/";
var SERVER_URL_AUTH = "https://api.motc.gov.qa/"
// var SERVER_URL_AUTH = "https://api.motc.gov.qa/";
var SERVER_URL_PAYMENT = "https://stgepayment.www.gov.qa/eGovPaymentWeb/";

//https://epayment.www.gov.qa/eGovPaymentWeb/SendPaymentAction

var helper = function () { }
var hf = new helper();


app.request.setup({
    async: localStorage.getItem('async') === 'true' ? true : false,
    cache: false,
    crossDomain: true,
    beforeSend: function () {
        if (localStorage.getItem('hideloader') !== 'false') {
            hf.showloader("Loading...");
        }
    },
    // headers: {
    //     'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJPd25lcklEIjoxNjQsImlhdCI6MTU0MTQ5ODIyMCwiZXhwIjoxNTQzMzk5MDIwfQ.uJGPBnJkAd7yyy3pa8-rV4QPMrhUatWHAsz2tp8_Bpd6ymVmn0mwJRj6pxKVMzwCrt8YZH_c5bYTn5sveet-Toscpeke2Mi9QkJyHfXjuvmqhj-hrcJRkzA25bgrkoHckHE_U3iUI3KSdEQuZZnNl_0wRzUgUraEU01l8GTUanTntXX3inZ4H62ubtwnoRsCmZnhMBZqrsZWBwvc4TYzyV_7_WLyt6V3MV57OhLo4XzU41YAn16_F4h3fp49wP1Oq58jC5tzZbugdpFUokkwxl0N05IVvU3pgDbEKgsDAgXyJHH128s6ZsGIoiEEZnawGJvzGk7M9UoI1ZXYu_tEHQ',
    //     "contentType": 'application/json',
    // },
    contentType: 'application/json',
    // duration: 80000,
    timeout: 20000,
    // dataType: "json",
    error: function (xhr, status) {
        console.log('error');
        console.log(status);
        //if (localStorage.getItem('hideloader') !== 'false') {
        hf.hideloader();
        //}

    },
    statusCode: {
        404: function (xhr) {
            alert('page not found');
        }
    },
    success: function () {
        if (localStorage.getItem('hideloader') !== 'false') {
            hf.hideloader();
        }

    },
})

helper.prototype.alert = function (text) {
    app.dialog.alert(test);
}
helper.prototype.showloader = function (text) {
    if (!text) text = "Loading...";
    app.preloader.show();
}
helper.prototype.hideloader = function (text) {
    app.preloader.hide();
}

helper.prototype.getData = function (url, params, callback) {

    app.request.get(SERVER_URL + url, params, function (data, status, xhr) {
        //console.log(data);

        callback(data);

    }, function (xhr, status) {
        console.log("error");
        console.log(xhr);
    });
}

helper.prototype.postData = function (serverURL, url, body, callback) {



    if (serverURL === 'login') {
        localStorage.setItem('async', false);
        // app.request.post(SERVER_URL_AUTH + url, body, function (data, status, success, error) {
        //     console.log(success);
        //     console.log(error);

        //     if (success.response === 'Something went wrong.' && success.requestUrl !== 'https://bvservices.malomatia.com/UploadedDocumentsGetItemByFileTypeAndGUID/') {
        //         app.dialog.alert('Error! Please try again.', 'Alert');
        //     }
        //     else {
        //         callback(data);
        //     }

        // });


        app.request.postJSON(SERVER_URL + url, body, function (data) {
            console.log("-----------------------");
            console.log(body);

            callback(data);
        }, function (err) {
            console.log(err);
        }
        );


    }
    if (serverURL === 'payment') {
        app.request.postJSON(SERVER_URL_PAYMENT + url, body, function (data, status, success, error) {

            console.log(success);
            console.log(error);

            if (success.response === 'Something went wrong.' && success.requestUrl !== 'https://bvservices.malomatia.com/UploadedDocumentsGetItemByFileTypeAndGUID/') {
                app.dialog.alert('Error! Please try again.', 'Alert');
            }
            else {
                callback(data);
            }

        });
    }
    else if (serverURL === true) {
        localStorage.setItem('async', true);



        app.request.postJSON(SERVER_URL + url, body, function (data) {
            callback(data);
        }, function (err) {
            console.log(err);
        }
        );

        // console.log(body);

        // app.request.post({
        //     url: "http://localhost:3010/" + url,
        //     method: "POST",
        //     data: {itemID: "3"},
        //     cache: false,
        //     crossDomain: true,
        //     contentType:"application/json",
        //     headers: {  
        //         "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOiJ0YWJpc2hpa3JhbTE5NDdAZ21haWwuY29tIiwiaWF0IjoxNTQwOTc0ODE5LCJleHAiOjE1NDI4NzU2MTl9.jx7oiy4FRKCTZ9Rfiuag2oorgrnIOROIpkla6_B-HXViYdjrSQLVijP_tVH7HmLwkrgPf4bl82FJPoTCSrzfoPWilkFjR82lPsgqd-JkU7Dx0_kO-Tn_nbRBAjfb_Q6hPoC3i4dCR5-fgUmHH93F9DDvhiEBIRVmib1uQuEFW9doaYFHYK22kdVCSFpsrlD-uccSrucLL9rN3ATuSFsdV8hwfTsR3hBL4EK1olrvj2w-KMezuYgE6PNHNoxeHfTn5mEGtE4M2q-z3fe5K9Ein7DygwDM4Ix9FZCkpIZBZwz48WtyVsqkAJA3XlEUWuesefrTm7vhovDWtIe5BiKi4g",
        //     },
        //     dataType:"json",
        //     success: function (data, status) {
        //         console.log(data);
        //     },
        //     error: function (xhr, status) {
        //         console.log(status);
        //         console.log(xhr);
        //     }
        // })
    }
    else if (serverURL === false) {
        localStorage.setItem('async', true);



        app.request.post(SERVER_URL_2 + url, body, function (data, status, success) {

            console.log(success);
            // console.log(error);
            console.log(status)

            if (success.response === 'Something went wrong.' && success.requestUrl !== 'https://bvservices.malomatia.com/UploadedDocumentsGetItemByFileTypeAndGUID/') {
                app.dialog.alert('Error! Please try again.', 'Alert');
            }
            else {
                callback(data);
            }

        });

    }

}






